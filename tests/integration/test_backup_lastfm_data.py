__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

import os
from pickle import PicklingError
from unittest.mock import patch

import pytest

from media_tools.backup_lastfm_data import main


@patch('pylast.LastFMNetwork')
@patch('pylast.User')
def test_main_fails_without_args(_, __):
    os.environ['PYLAST_API_KEY'] = 'mock'
    os.environ['PYLAST_API_SECRET'] = 'mock'  # nosec
    os.environ['PYLAST_USERNAME'] = 'mock'
    with pytest.raises(PicklingError):
        main([])
