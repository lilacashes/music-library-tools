__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

import logging
import re
from pathlib import Path
from typing import List

import pytest

from media_tools.util import JunkRemover


CURRENTLY_WORKING_FOLDERS = [
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
    'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'tape', '#'
]
CURRENTLY_NOT_WORKING_FOLDERS = [
    '.', 'mix', 'va', 'spoken'
]


@pytest.mark.parametrize('start_letter', CURRENTLY_WORKING_FOLDERS)
def test_good_real_world_cases(start_letter: str) -> None:
    fails = _test_real_world_cases(start_letter)
    assert [] == fails


@pytest.mark.parametrize('start_letter', CURRENTLY_NOT_WORKING_FOLDERS)
@pytest.mark.xfail
def test_bad_real_world_cases(start_letter: str) -> None:
    # it's not possible everywhere, so don't really test for it, but write a warning
    fails = _test_real_world_cases(start_letter)
    logging.warning(fails)
    assert [] == fails


def _test_real_world_cases(start_letter: str) -> List:
    fails = []
    basedir = Path.home() / 'Music' / start_letter
    fixed = JunkRemover(basedir).fix_commands()
    logging.info('%s matched files', len(fixed))
    for _, replacement, patterns in fixed:
        core = Path(replacement).stem
        if '_' in core:
            fails.append((core, patterns))
        if '  ' in core:
            fails.append((core, patterns))
        if '--' in core:
            fails.append((core, patterns))
        if '- -' in core:
            fails.append((core, patterns))
        if core.endswith('-'):
            fails.append((core, patterns))
        if core.startswith('-'):
            fails.append((core, patterns))
        if core.endswith(' '):
            fails.append((core, patterns))
        if core.startswith(' '):
            fails.append((core, patterns))
        # ideal filename pattern
        if not re.match(r'^\d{1,3}\s?-\s?.*$', core):
            if not re.match(r'\D.*\D', core) and not re.match(r'^\d{1,3}$', core):
                fails.append((core, patterns))
        if re.match(r'-\w{1,4}$', core):
            fails.append((core, patterns))

    return fails
