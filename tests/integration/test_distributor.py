__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

import pytest

from media_tools.util.buying.distributor import Amazon, Beatport
from media_tools.util.buying.track import Track


def test_present_track_beatport():
    check_present_track(Beatport(), 'Steve Lacy', 'Zoo Original Mix', fail_tolerant=True)


def test_absent_track_beatport():
    check_absent_track(
        Beatport(), 'I dont exist on Beatport', 'Nor will I ever', fail_tolerant=True
    )


def test_present_track_amazon():
    check_present_track(Amazon(), 'Madonna', 'True Blue', fail_tolerant=True)


def test_absent_track_amazon():
    check_absent_track(Amazon(), 'I dont exist on Amazon', 'Nor will I ever')


def check_track_condition(
        distributor, artist: str, title: str, condition: bool, fail_tolerant: bool = False
):
    Track.setup((distributor,), None)
    present = distributor.is_present(Track(artist, title))
    if fail_tolerant and present != condition:
        pytest.xfail(f'{artist} - {title} failed for {type(distributor).__name__}')
    assert condition == present


def check_present_track(distributor, artist: str, title: str, fail_tolerant: bool = False):
    check_track_condition(distributor, artist, title, True, fail_tolerant)


def check_absent_track(distributor, artist: str, title: str, fail_tolerant: bool = False):
    check_track_condition(distributor, artist, title, False, fail_tolerant)
