__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

from pathlib import Path
from shutil import copytree
from tempfile import TemporaryDirectory
from typing import Generator

import pytest

from media_tools.play_time import LengthStore

NUM_COPIED_TREES = 10


@pytest.fixture
def data_path() -> Path:
    return Path(__file__).resolve().parent / 'data' / 'print_length'


def make_length_store(path: Path) -> LengthStore:
    length_store = LengthStore(path, recursive=True)
    length_store.scan()
    return length_store


@pytest.fixture
def length_store(data_path: Path) -> LengthStore:
    return make_length_store(data_path)


@pytest.fixture
def multiple_trees(data_path: Path) -> Generator[Path, None, None]:
    temp = TemporaryDirectory()
    temp_path = Path(temp.name)
    for i in range(NUM_COPIED_TREES):
        copytree(data_path, temp_path / str(i))
    yield temp_path


def test_length_correct(data_path: Path, length_store: LengthStore):
    assert length_store.lengths[data_path].seconds == 17


def test_all_backends_used_once(data_path: Path, length_store: LengthStore):
    assert len(length_store.backends_used) == 4
    assert all(len(backend) == 1 for backend in length_store.backends_used.values())


@pytest.mark.parametrize('reader,test_file,length', [
    ('Audioread', 'IfCouldntPlayMusic.mp3', 6), ('Eyed3', 'MissWorld1.mp3', 1),
    ('Mutagen', 'white sheep.mp3', 3), ('Soundfile', 'Heuchler.ogg', 6)
])
def test_data_read_correctly(data_path: Path, length_store: LengthStore, reader, test_file, length):
    test_path = data_path / reader.lower() / test_file
    assert test_path in length_store.backends_used[reader + 'LengthReader']
    assert int(length_store.backends_used[reader + 'LengthReader'][test_path]) == length


def test_one_track_has_error(length_store: LengthStore):
    assert len(length_store.errors) == 1


@pytest.mark.parametrize('error_file,length', [('intro.mp3', 2176)])
def test_error_read_correctly(data_path: Path, length_store: LengthStore, error_file, length):
    error_file_path = data_path / 'error' / error_file
    assert error_file_path in length_store.errors
    assert length_store.errors[error_file_path] == length


def test_length_correct_multiple_trees(multiple_trees: Path):
    length_store = make_length_store(multiple_trees)
    assert length_store.lengths[multiple_trees].seconds == 175


def test_backends_used_multiple_trees(multiple_trees: Path):
    length_store = make_length_store(multiple_trees)
    assert len(length_store.backends_used) == 4
    assert all(len(backend) == NUM_COPIED_TREES for backend in length_store.backends_used.values())
    assert len(length_store.errors) == NUM_COPIED_TREES
