__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Tuple
from unittest.mock import Mock, patch

import pytest

from media_tools.util.mixcloud import create_mix, MixPath, Mix, MultiMix
from media_tools.util.mixcloud.constants import DEFAULT_AUDIO_FILE_TYPES

MOCK_ACCESS_TOKEN = 'mock'  # nosec


@pytest.fixture
def test_dir() -> TemporaryDirectory:
    return TemporaryDirectory()


@pytest.fixture
def pattern() -> Tuple[str, ...]:
    return tuple(f'*.{ext}' for ext in DEFAULT_AUDIO_FILE_TYPES)


@pytest.fixture
def mix_path(test_dir: TemporaryDirectory, pattern: Tuple[str, ...]) -> MixPath:
    return MixPath(Path(test_dir.name), pattern)


@pytest.fixture
def mix(mix_path: MixPath) -> Mix:
    return create_mix(mix_path, MOCK_ACCESS_TOKEN, crossfade_ms=0)


@pytest.fixture
def multi_mix(mix_path: MixPath) -> MultiMix:
    return MultiMix(mix_path, MOCK_ACCESS_TOKEN, total_length=4, crossfade_ms=0)


@patch('logging.error')
def test_upload_runs(error: Mock, mix: Mix) -> None:
    """calls Mixcloud API with auth token "mock" - will exit() due to OAuthException"""
    mix.export()
    with pytest.raises(SystemExit):
        mix.upload()
    error.assert_called_once()


@patch('logging.error')
def test_multimix_upload_runs(error: Mock, multi_mix: MultiMix) -> None:
    """calls Mixcloud API with auth token "mock" - will exit() due to OAuthException"""
    multi_mix.export()
    with pytest.raises(SystemExit):
        multi_mix.upload()
    error.assert_called_once()
