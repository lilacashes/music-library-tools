from media_tools.util.buying.track import Track


BEATPORT_URL = 'http://www.beatport.com/search?q="word1+word2"+"word3+word4+word5"'
AMAZON_URL = 'https://smile.amazon.de/s/ref=nb_sb_noss?url=search-alias%3Daps&' \
    'field-keywords="word1+word2"+"word3+word4+word5"'


Track.setup((), None)
TRACK = Track('word1 word2', 'word3 word4 word5')
