__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

from pathlib import Path
from unittest.mock import Mock, patch

import pytest

from media_tools.util.buying.distributor import Amazon, Beatport
from media_tools.util.buying.track import Track
from media_tools.util.buying.trackdb import TrackDB

from tests.buying_common import BEATPORT_URL, AMAZON_URL


@pytest.fixture
def trackdb_path(testpath: Path) -> Path:
    return testpath / 'tracks.pickle'


@pytest.fixture
def trackdb(trackdb_path: Path) -> TrackDB:
    trackdb = TrackDB(trackdb_path)
    Track.setup((Beatport(), Amazon()), trackdb)
    return trackdb


@pytest.fixture
def track() -> Track:
    return Track('word1 word2', 'word3 word4 word5')


@patch.object(Beatport, 'is_present', return_value=True)
@patch('webbrowser.open')
def test_buy_opens_browser_with_correct_url_beatport(
        mock_open: Mock, beatport: Mock, trackdb: TrackDB, track: Track
):
    track.buy()
    mock_open.assert_called_once_with(BEATPORT_URL)


@patch.object(Beatport, 'is_present', return_value=False)
@patch.object(Amazon, 'is_present', return_value=True)
@patch('webbrowser.open')
def test_buy_opens_browser_with_correct_url_amazon(
        mock_open: Mock, amazon: Mock, beatport: Mock, trackdb: TrackDB, track: Track
):
    track.buy()
    mock_open.assert_called_once_with(AMAZON_URL)


@patch.object(Beatport, 'is_present', return_value=True)
@patch('webbrowser.open')
def test_track_present_in_db_after_buy(
        mock_open: Mock, beatport: Mock, trackdb: TrackDB, track: Track
):
    track.buy()
    assert track in trackdb


@patch.object(Beatport, 'is_present', return_value=False)
@patch.object(Amazon, 'is_present', return_value=False)
@patch('webbrowser.open')
def test_track_not_present_if_no_buy_url(
        mock_open: Mock, amazon: Mock, beatport: Mock, trackdb: TrackDB, track: Track
):
    track.buy()
    assert track not in trackdb


@patch.object(Beatport, 'is_present', return_value=True)
@patch('webbrowser.open')
def test_track_db_is_persistent(mock_open: Mock, beatport: Mock, trackdb_path: Path):
    with TrackDB(trackdb_path) as trackdb:
        Track.setup((Beatport(), Amazon()), trackdb)
        Track('word1 word2', 'word3 word4 word5').buy()
    assert trackdb_path.exists()
    assert trackdb_path.is_file()


@patch.object(Beatport, 'is_present', return_value=True)
@patch('webbrowser.open')
def test_persistent_track_db_contains_previous_entry(
        mock_open: Mock, beatport: Mock, trackdb_path: Path
):
    with TrackDB(trackdb_path) as trackdb:
        Track.setup((Beatport(), Amazon()), trackdb)
        Track('word1 word2', 'word3 word4 word5').buy()
    with TrackDB(trackdb_path) as trackdb:
        Track.setup((Beatport(), Amazon()), trackdb)
        assert Track('word1 word2', 'word3 word4 word5') in trackdb


@patch.object(Beatport, 'is_present', return_value=True)
@patch('webbrowser.open')
def test_persistent_track_db_is_backed_up_before_write(
        mock_open: Mock, beatport: Mock, trackdb_path: Path
):
    with TrackDB(trackdb_path) as trackdb:
        Track.setup((Beatport(), Amazon()), trackdb)
        Track('word1 word2', 'word3 word4 word5').buy()
    suffix = trackdb.suffix_with_appended_timestamp(trackdb_path)
    with TrackDB(trackdb_path) as trackdb:
        Track.setup((Beatport(), Amazon()), trackdb)
    assert trackdb_path.with_suffix(suffix).exists()
