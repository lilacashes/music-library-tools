import os
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Generator, List, Tuple

import pytest

from media_tools.copy_from_playlist import AudaciousTools, PlaylistCopier
from .conftest import create_file

__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

# as defined in data/audacious_config/playlists/0002.audpl
TMPDIR = Path('/tmp')  # nosec


@pytest.fixture
def file_names() -> Tuple[str, ...]:
    return '01 - Test A.mp3', 'Test B.mp3'


@pytest.fixture
def audacious(
        local_path: Path, testdir: TemporaryDirectory, file_names: List[str]
) -> Generator[AudaciousTools, None, None]:
    for file in file_names:
        create_file(Path(testdir.name).joinpath(file))

    for file in file_names:
        assert file in os.listdir(testdir.name)

    yield AudaciousTools(str(local_path))

    for file in file_names:
        try:
            Path(TMPDIR).joinpath(file).unlink()
        except FileNotFoundError:
            pass


def test_move_files_to_original_places(testpath: Path, audacious: AudaciousTools) -> None:
    # get list of files which should be copied to TMPDIR first, to compare against
    # path TMPDIR is hardwired in 0002.audpl, so can't use TemporaryDirectory() here
    PlaylistCopier(audacious, '0002').copy_playlist(0, testpath)
    copied_files = [Path(f) for f in os.listdir(testpath)]
    # ensure they are wiped so the test is not influenced by files remaining from earlier runs
    clean_tmp_dir(copied_files)
    assert not any(file.exists() for file in copied_files)

    try:
        back_copier = PlaylistCopier(audacious, '0002')
        back_copier.move_files_to_original_places(music_dir=TMPDIR)

        for file in copied_files:
            assert (TMPDIR / file.name).is_file()
    except FileNotFoundError:
        pytest.xfail('temporary directory went away before test finished')
    finally:
        clean_tmp_dir(copied_files)


def clean_tmp_dir(copied_files: List[Path]) -> None:
    for file in copied_files:
        if (TMPDIR / file).is_file():
            (TMPDIR / file).unlink()


def test_move_files_to_original_places_creates_target_files(
        audacious: AudaciousTools, file_names: List[str], testpath: Path
) -> None:
    copier = PlaylistCopier(audacious, '0002')
    copier.move_files_to_original_places(music_dir=testpath)
    for file in file_names:
        assert file in os.listdir(TMPDIR)


def test_move_files_to_original_places_deletes_source_dir(
        audacious: AudaciousTools, testpath: Path
) -> None:
    copier = PlaylistCopier(audacious, '0002')
    copier.move_files_to_original_places(music_dir=testpath)
    if testpath.exists():
        assert not any(testpath.iterdir())
