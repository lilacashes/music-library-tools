__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

from pathlib import Path
from typing import Callable

import pytest

from media_tools.clean_filenames import FilenameCleaner
from media_tools.util.media_extensions import MEDIA_EXTENSIONS
from media_tools.util.extension_lowercaser import ExtensionLowercaser

from .conftest import create_files

NUM_TESTS = 10


@pytest.fixture
def lowercaser(testpath: Path) -> ExtensionLowercaser:
    return ExtensionLowercaser(testpath)


@pytest.mark.parametrize('extension', MEDIA_EXTENSIONS)
def test_find_uppercase_extensions(extension: str, lowercaser: ExtensionLowercaser, testpath: Path):
    create_files(testpath, '{:02d} - blah {}.' + extension.upper(), NUM_TESTS)
    for file in testpath.iterdir():
        assert file in lowercaser.find_uppercase_extensions()


@pytest.mark.parametrize('extension', MEDIA_EXTENSIONS)
def test_do_not_find_lowercase_extensions(
        extension: str, lowercaser: ExtensionLowercaser, testpath: Path
):
    create_files(testpath, '{:02d} - blah {}.' + extension, NUM_TESTS)
    assert [] == lowercaser.find_uppercase_extensions()


@pytest.mark.parametrize('extension', MEDIA_EXTENSIONS)
def test_find_mixed_case_extensions(
        extension: str, lowercaser: ExtensionLowercaser, testpath: Path
):
    create_files(testpath, '{:02d} - blah {}.' + extension.capitalize(), NUM_TESTS)
    for file in testpath.iterdir():
        assert file in lowercaser.find_uppercase_extensions()


@pytest.mark.parametrize('extension', MEDIA_EXTENSIONS)
def test_get_fix_commands(extension: str, lowercaser: ExtensionLowercaser, testpath: Path):
    create_files(testpath, '{:02d} - blah {}.' + extension.upper(), NUM_TESTS)
    for command in lowercaser.fix_commands():
        assert command.src.suffix != command.src.suffix.lower()
        assert command.dest.suffix == command.dest.suffix.lower()


@pytest.mark.parametrize('extension', MEDIA_EXTENSIONS)
def test_lowercase_exts_lowercases_extensions(
        extension: str, cleaner: FilenameCleaner, testpath: Path
):
    create_files(testpath, '{:02d} - blah {}.' + extension.upper(), NUM_TESTS)
    _perform_and_check_cleaning(cleaner, testpath, lambda s: s.name.split('.')[1].islower())


@pytest.mark.parametrize('extension', MEDIA_EXTENSIONS)
def test_lowercase_exts_stays_in_same_folder(
        extension: str, cleaner: FilenameCleaner, testpath: Path
):
    create_files(testpath, '{:02d} - blah {}.' + extension.upper(), NUM_TESTS)
    _perform_and_check_cleaning(cleaner, testpath, lambda s: s.parent == testpath)


def _perform_and_check_cleaning(
        cleaner: FilenameCleaner, testpath: Path, assertion: Callable[[Path], bool]
) -> None:
    _perform_cleaning(cleaner)
    for file in testpath.iterdir():
        assert assertion(file)


def _perform_cleaning(cleaner: FilenameCleaner) -> None:
    cleaner.lowercase_exts()
