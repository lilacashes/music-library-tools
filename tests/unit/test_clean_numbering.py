__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

import re
from os import listdir
from pathlib import Path

import pytest

from media_tools.clean_filenames import FilenameCleaner
from .conftest import create_files

NUM_TESTS = 3

NUMBER_CASE_TEMPLATES = [
    '{:02d}a{}.mp3', '[{:02d}]{}.mp3', '{:02d}_{}.mp3', '-{:02d}. {}.mp3', '-{:02d}-{}.mp3',
    '{:02d}--{}.mp3', '{:02d}-   {}.mp3', '{:02d}-{}.mp3', '{:02d}- {}.mp3', '{:02d}.   {}.mp3',
    '{:02d}.{}.mp3', '{:02d}. {}.mp3', '{:02d}   {}.mp3', '{:02d} {}.mp3'
]
SIDE_AND_NUMBER_CASE_TEMPLATES = [
    'a{:01d} {}.mp3', 'a{:01d}-{}.mp3', 'a{:01d}.{}.mp3', 'a{:01d}{}.mp3',
    '[a{:01d}]{}.mp3', 'a{:01d}]{}.mp3', '(a{:01d}){}.mp3'
]


@pytest.mark.parametrize('pattern', [
    '{:02d} {}.mp3',     # space_after_number
    '{:02d}   {}.mp3',   # multiple_spaces_after_number
    '{:02d}. {}.mp3',    # dot_after_number
    '{:02d}.{}.mp3',     # dot_and_no_space_after_number
    '{:02d}.   {}.mp3',  # dot_and_multiple_spaces_after_number
    '{:02d}- {}.mp3',    # dash_after_number
    '{:02d}-{}.mp3',     # dash_and_no_space_after_number
    '{:02d}-   {}.mp3',  # dash_and_multiple_spaces_after_number
    '{:02d}--{}.mp3',    # double_dash_after_number
    '-{:02d}-{}.mp3',    # number_within_dashes
    '-{:02d}. {}.mp3',   # dash_number_dot
    '{:02d}_{}.mp3',     # number_underscore
    '[{:02d}]{}.mp3',    # bracketed_number
    '({:02d}){}.mp3',    # braced_number
    '{:02d}a{}.mp3',     # text_immediately_after_number
    '{:02d} – {}.mp3',   # en_dash
    '{:02d}–{}.mp3',     # en_dash_no_space
    '{:02d} — {}.mp3',   # em_dash
    '{:02d}—{}.mp3',     # em_dash_no_space
])
def test_pattern_is_fixed(pattern: str, cleaner: FilenameCleaner, testpath: Path) -> None:
    create_files(testpath, pattern, NUM_TESTS)
    _perform_and_check_cleaning(cleaner, testpath)


@pytest.mark.parametrize('pattern, regex', [
    ('a{:01d} {}.mp3', r'^a\d - \w+\.mp3$'),  # space_after_side_and_number
    ('a{:01d}-{}.mp3', r'^a\d - \w+\.mp3$'),  # dash_after_side_and_number
    ('a{:01d}.{}.mp3', r'^a\d - \w+\.mp3$'),  # dot_after_side_and_number
    ('a{:01d}{}.mp3', r'^a\d - \w+\.mp3$'),  # text_immediately_after_side_and_number
    ('[a{:01d}]{}.mp3', r'^a\d - \w+\.mp3$'),  # bracketed_side_and_number
    ('a{:01d}]{}.mp3', r'^a\d - \w+\.mp3$'),  # bracket_after_side_and_number
    ('(a{:01d}){}.mp3', r'^a\d - \w+\.mp3$'),  # braced_side_and_number
    ('{:02d}.mp3', r'^\d\d\.mp3$'),  # numbers_only_are_left_alone
    ('10-{:01d} {}.mp3', r'^\d\d-\d - \w+\.mp3$'),  # ttc_numbering_scheme
    ('10-{:02d} {}.mp3', r'^\d\d-\d\d - \w+\.mp3$'),  # ttc_numbering_scheme 2
    ('10{:02d} {}.mp3', r'^\d\d\d\d - \w+\.mp3$'),  # ttc_numbering_scheme 3
    ('1-{:02d} - {}.mp3', r'^\d-\d\d - \w+\.mp3$'),  # leading_cd_id
    ('1-{:02d} {}.mp3', r'^\d-\d\d - \w+\.mp3$'),  # leading_cd_id
    ('1-{:02d}-{}.mp3', r'^\d-\d\d - \w+\.mp3$'),  # leading_cd_id
    ('1.{:02d} - {}.mp3', r'^\d-\d\d - \w+\.mp3$'),  # leading_cd_id
    ('1.{:02d} {}.mp3', r'^\d-\d\d - \w+\.mp3$'),  # leading_cd_id
    ('1.{:02d}-{}.mp3', r'^\d-\d\d - \w+\.mp3$'),  # leading_cd_id
])
def test_pattern_is_fixed_to_given_regex(
        pattern: str, regex: str, cleaner: FilenameCleaner, testpath: Path
) -> None:
    create_files(testpath, pattern, NUM_TESTS)
    _perform_and_check_cleaning(cleaner, testpath, regex)


def test_doubled_numbers(cleaner: FilenameCleaner, testpath: Path) -> None:
    create_files(testpath, '{0:02d}{0:02d} - {1}.mp3', NUM_TESTS)
    _perform_and_check_cleaning(cleaner, testpath, r'^\d\d - \w+\.mp3$')


@pytest.mark.parametrize('pattern', NUMBER_CASE_TEMPLATES)
def test_all_number_stuff_with_space_first(
        pattern: str, cleaner: FilenameCleaner, testpath: Path
) -> None:
    create_files(testpath, ' ' + pattern, NUM_TESTS)
    _perform_and_check_cleaning(cleaner, testpath)


@pytest.mark.parametrize('pattern', SIDE_AND_NUMBER_CASE_TEMPLATES)
def test_all_side_and_number_stuff_with_space_first(
        pattern: str, cleaner: FilenameCleaner, testpath: Path
) -> None:
    create_files(testpath, ' ' + pattern, NUM_TESTS)
    _perform_and_check_cleaning(cleaner, testpath, r'^a\d - \w+\.mp3$')


@pytest.mark.parametrize('pattern', NUMBER_CASE_TEMPLATES)
def test_all_number_stuff_with_flac(
        pattern: str, cleaner: FilenameCleaner, testpath: Path
) -> None:
    create_files(testpath, pattern.replace('mp3', 'flac'), NUM_TESTS)
    _perform_and_check_cleaning(cleaner, testpath, r'^\d+ - \w+\.flac$')


@pytest.mark.parametrize('pattern', SIDE_AND_NUMBER_CASE_TEMPLATES)
def test_all_side_and_number_stuff_with_flac(
        pattern: str, cleaner: FilenameCleaner, testpath: Path
) -> None:
    create_files(testpath, pattern.replace('mp3', 'flac'), NUM_TESTS)
    _perform_and_check_cleaning(cleaner, testpath, r'^a\d - \w+\.flac$')


@pytest.mark.parametrize('pattern', NUMBER_CASE_TEMPLATES)
def test_all_number_stuff_does_not_rename_nonmusical_files(
        pattern: str, cleaner: FilenameCleaner, testpath: Path
) -> None:
    create_files(testpath, pattern.replace('mp3', 'txt'), NUM_TESTS)
    _perform_cleaning(cleaner)
    for file in listdir(testpath):
        assert re.search(r'^\d+ - \w+\.txt$', file) is None


@pytest.mark.parametrize('pattern', SIDE_AND_NUMBER_CASE_TEMPLATES)
def test_all_side_and_number_stuff_does_not_rename_nonmusical_files(
        pattern: str, cleaner: FilenameCleaner, testpath: Path
) -> None:
    create_files(testpath, pattern.replace('mp3', 'txt'), NUM_TESTS)
    _perform_cleaning(cleaner)
    for file in listdir(testpath):
        assert re.search(r'^\d+ - \w+\.txt$', file) is None


def test_unnumbered_files_are_not_renamed(cleaner: FilenameCleaner, testpath: Path) -> None:
    create_files(testpath, 'abc{:02d}{}.mp3', NUM_TESTS)
    _perform_cleaning(cleaner)
    for file in listdir(testpath):
        assert re.search(r'^\d+ - \w+\.mp3$', file) is None


def _perform_and_check_cleaning(
        cleaner: FilenameCleaner, testpath: Path, regex: str = r'^\d+ - \w+\.mp3$'
) -> None:
    _perform_cleaning(cleaner)
    for file in listdir(testpath):
        assert re.search(regex, file) is not None


def _perform_cleaning(cleaner: FilenameCleaner) -> None:
    cleaner.clean_numbering()
