__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

from argparse import Namespace
from unittest.mock import Mock, patch

import pytest

from media_tools.buy_most_played import buy_exactly
from media_tools.util.buying.last_fm import LastFM
from media_tools.util.buying.track import Track

Track.setup(Mock(), None)


@pytest.fixture
def api():
    return LastFM(Mock(), Mock())


@patch('media_tools.util.buying.track.Track.buy_url', return_value='ok')
@patch('media_tools.buy_most_played.get_tracks_chunk', return_value=[Track('Artist 1', 'Title 1')])
def test_buy_exactly_runs(_, __, api) -> None:
    buy_exactly(api, Namespace(buy_up_to=1))


@patch('media_tools.util.buying.track.Track.buy_url', return_value='ok')
@patch(
    'media_tools.buy_most_played.get_tracks_chunk',
    side_effect=[[Track('Artist 1', 'Title 1'), Track('Artist 2', 'Title 2')]]
)
def test_buy_exactly_buys_fewer_than_available_if_requested(_, __, api) -> None:
    assert 1 == buy_exactly(api, Namespace(buy_up_to=1))


@patch('media_tools.util.buying.track.Track.buy_url', return_value='ok')
@patch(
    'media_tools.buy_most_played.get_tracks_chunk',
    side_effect=[[Track('Artist 1', 'Title 1'), Track('Artist 2', 'Title 2')]]
)
def test_buy_exactly_buys_exact_number(_, __, api) -> None:
    assert 2 == buy_exactly(api, Namespace(buy_up_to=2))


@patch('media_tools.util.buying.track.Track.buy_url', return_value='ok')
@patch(
    'media_tools.buy_most_played.get_tracks_chunk',
    side_effect=[[Track('Artist 1', 'Title 1'), Track('Artist 2', 'Title 2')]]
)
def test_buy_exactly_buys_fewer_than_requested_if_not_enough_available(_, __, api) -> None:
    with pytest.raises(StopIteration):
        buy_exactly(api, Namespace(buy_up_to=3))
