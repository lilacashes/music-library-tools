__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

from unittest.mock import patch

import pytest

from media_tools.util.buying.distributor import Amazon, Beatport
from media_tools.util.buying.track import Track

from tests.buying_common import BEATPORT_URL, AMAZON_URL


@pytest.fixture
def no_distributors() -> None:
    Track.setup((), None)


@pytest.fixture
def with_distributors() -> None:
    Track.setup((Beatport(), Amazon()), None)


def test_init_fails_without_setup():
    # no way to keep this field pristine with random test order, so need to manually set it
    Track.distributors = None
    with pytest.raises(ValueError):
        Track('artist', 'title', 10)


def test_init_succeeds(no_distributors):
    track = Track('artist', 'title', 10)
    assert track.artist == 'artist'
    assert track.title == 'title'
    assert track.play_count == 10


def test_from_dict_fails_without_proper_fields(no_distributors):
    with pytest.raises(KeyError):
        Track.from_dict({})


def test_multiple_words_are_quoted_in_properties(no_distributors):
    track = Track('word1 word2', 'word3 word4 word5')
    assert track.artist == 'word1+word2'
    assert track.title == 'word3+word4+word5'


def test_same_track_with_different_playcounts_is_equal(no_distributors):
    track1 = Track('artist', 'title', 1)
    track2 = Track('artist', 'title', 2)
    assert track1 == track2


def test_tracks_with_different_titles_are_unequal(no_distributors):
    track1 = Track('artist', 'title 1', 1)
    track2 = Track('artist', 'title 2', 2)
    assert track1 != track2


@patch.object(Beatport, 'is_present', return_value=True)
def test_buy_url_when_present_on_beatport(_, with_distributors):
    assert Track('word1 word2', 'word3 word4 word5').buy_url() == BEATPORT_URL


@patch.object(Beatport, 'is_present', return_value=False)
@patch.object(Amazon, 'is_present', return_value=True)
def test_buy_url_when_not_present_on_beatport_but_amazon(_, __, with_distributors):
    assert Track('word1 word2', 'word3 word4 word5').buy_url() == AMAZON_URL


@patch.object(Beatport, 'is_present', return_value=True)
@patch.object(Amazon, 'is_present', return_value=True)
def test_buy_url_when_present_on_both_beatport_and_amazon(_, __, with_distributors):
    assert Track('word1 word2', 'word3 word4 word5').buy_url() == BEATPORT_URL


@patch.object(Beatport, 'is_present', return_value=False)
@patch.object(Amazon, 'is_present', return_value=False)
def test_buy_url_when_not_present_on_either_beatport_or_amazon(_, __, with_distributors):
    assert not Track('word1 word2', 'word3 word4 word5').buy_url()
