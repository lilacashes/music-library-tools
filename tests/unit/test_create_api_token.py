__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

from http.server import HTTPServer
from unittest.mock import Mock, patch

import pytest

from media_tools.util.mixcloud.create_api_token import Step1, Step2
from media_tools.util.mixcloud.oauth_parameter_receiver import ReceivedCode

MOCK_CODE = 'XXXX'


@pytest.fixture
def step_2() -> Step2:
    return Step2(Mock())


@patch('webbrowser.open_new_tab')
def test_step_1_opens_mixcloud_api_url(open_new_tab: Mock):
    Step1().run()
    open_new_tab.assert_called_once_with('https://www.mixcloud.com/developers/create')


@patch('webbrowser.open_new_tab')
@patch.object(HTTPServer, 'serve_forever')
def test_step_2_opens_tab(_: Mock, open_new_tab: Mock, step_2: Step2):
    step_2.run()
    open_new_tab.assert_called_once()


@patch('webbrowser.open_new_tab')
@patch.object(HTTPServer, 'serve_forever')
def test_step_2_serves(serve_forever: Mock, _: Mock, step_2: Step2):
    step_2.run()
    serve_forever.assert_called_once()


@patch('webbrowser.open_new_tab')
@patch.object(HTTPServer, 'serve_forever', side_effect=ReceivedCode(MOCK_CODE))
def test_step_2_sets_code(_: Mock, __: Mock, step_2: Step2):
    step_2.run()
    assert step_2.oauth_code is not None
    assert step_2.oauth_code == MOCK_CODE


@patch('webbrowser.open_new_tab')
@patch.object(HTTPServer, 'serve_forever', side_effect=KeyboardInterrupt)
def test_step_2_interrupted_does_not_set_code(_: Mock, __: Mock, step_2: Step2):
    step_2.run()
    assert step_2.oauth_code is None


@patch('webbrowser.open_new_tab')
@patch.object(HTTPServer, 'serve_forever', side_effect=KeyError)
def test_step_2_bad_response_does_not_set_code(_: Mock, __: Mock, step_2: Step2):
    step_2.run()
    assert step_2.oauth_code is None
