__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

from datetime import datetime
from unittest.mock import Mock, patch

from media_tools.util.scrobbles import LastFMUser, year_range

import pylast
import pytest

DAY_SECONDS = 24 * 60 * 60
NORMAL_YEAR_SECONDS = 365 * DAY_SECONDS


@pytest.fixture()
def mock_user() -> pylast.User:
    return pylast.User(Mock(), Mock())


@patch.object(pylast.User, 'get_unixtime_registered', return_value=0)
def test_start_year_1970(_, mock_user: pylast.User):
    assert LastFMUser(mock_user).start_year() == 1970


@patch.object(pylast.User, 'get_unixtime_registered', return_value=NORMAL_YEAR_SECONDS)
def test_start_year_1971(_, mock_user: pylast.User):
    assert LastFMUser(mock_user).start_year() == 1971


def test_year_range():
    assert year_range(None) == dict(time_from=None, time_to=None)
    assert year_range(1970) == dict(time_from=0, time_to=NORMAL_YEAR_SECONDS - 1)
    assert year_range(1972) == dict(
        time_from=2 * NORMAL_YEAR_SECONDS, time_to=3 * NORMAL_YEAR_SECONDS + DAY_SECONDS - 1
    )


@patch.object(pylast.User, 'get_unixtime_registered', return_value=0)
def test_get_year_list(mock_user):
    assert LastFMUser(mock_user).get_year_list(None) == [None]
    assert LastFMUser(mock_user).get_year_list(1970) == [1970]
    assert LastFMUser(mock_user).get_year_list(0) == range(1970, datetime.now().year)
