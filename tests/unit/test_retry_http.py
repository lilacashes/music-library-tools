__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

import urllib.request
from unittest.mock import patch, call
from urllib.error import HTTPError

from media_tools.util.buying.retry_http import retry_http, MAX_TIMES_TRY, WAIT_RETRY_SECS

FILE_URL = 'file:///tmp'
HTTP_ERROR = HTTPError(FILE_URL, 404, 'Planned HTTPError', None, None)


@patch('urllib.request.urlopen')
def test_no_retry(urlopen):
    _make_test_object().test_base_case()
    urlopen.assert_called_with(FILE_URL)


@patch('time.sleep')
@patch('urllib.request.urlopen')
def test_retry_no_args_no_exception(urlopen, sleep):
    _make_test_object().test_no_args()
    urlopen.assert_called_once_with(FILE_URL)


@patch('time.sleep')
@patch('urllib.request.urlopen')
def test_retry_request_args(urlopen, sleep):
    _make_test_object().test_args(1, 2)
    urlopen.assert_called_once_with(FILE_URL)


@patch('time.sleep')
@patch('urllib.request.urlopen')
def test_retry_request_kwargs(urlopen, sleep):
    _make_test_object().test_kwargs(arg1=1, arg2=2)
    urlopen.assert_called_once_with(FILE_URL)


@patch('time.sleep')
@patch('urllib.request.urlopen', side_effect=HTTP_ERROR)
def test_retry_with_exception(urlopen, sleep):
    assert not _make_test_object().test_no_args()
    urlopen.assert_called_with(FILE_URL)
    assert MAX_TIMES_TRY == urlopen.call_count


@patch('time.sleep')
@patch('urllib.request.urlopen')
def test_retry_exception_then_success(urlopen, sleep):
    def raise_exception_once(*args, **kwargs):
        raise_exception_once.exception_count += 1
        if raise_exception_once.exception_count < 2:
            raise HTTP_ERROR

    raise_exception_once.exception_count = 0

    urlopen.side_effect = raise_exception_once
    _make_test_object().test_no_args()
    urlopen.assert_called_with(FILE_URL)
    assert 2 == urlopen.call_count


@patch('time.sleep')
@patch('urllib.request.urlopen', side_effect=HTTP_ERROR)
def test_max_times_try(urlopen, sleep):
    assert not _make_test_object(max_times_try=3).test_no_args()
    assert 3 == urlopen.call_count


@patch('time.sleep')
@patch('urllib.request.urlopen', side_effect=HTTP_ERROR)
@patch('builtins.print')
def test_silent(print, urlopen, sleep):
    _make_test_object(silent=False).test_no_args()
    assert MAX_TIMES_TRY == print.call_count


@patch('time.sleep')
@patch('urllib.request.urlopen', side_effect=HTTP_ERROR)
def test_retry_wait(urlopen, sleep):
    _make_test_object(wait_retry_secs=1).test_no_args()
    sleep.assert_called_once_with(1)


@patch('time.sleep')
@patch('urllib.request.urlopen', side_effect=HTTP_ERROR)
def test_retry_wait_progressive(urlopen, sleep):
    _make_test_object(max_times_try=3, wait_retry_progressive=True).test_no_args()
    sleep.assert_has_calls([call(WAIT_RETRY_SECS), call(2 * WAIT_RETRY_SECS)])


def _make_test_object(**decorator_args):
    class TestClass:
        def test_base_case(self):
            urllib.request.urlopen(FILE_URL)

        @retry_http(**decorator_args)
        def test_no_args(self):
            urllib.request.urlopen(FILE_URL)

        @retry_http(**decorator_args)
        def test_args(self, arg1, arg2):
            urllib.request.urlopen(FILE_URL)

        @retry_http(**decorator_args)
        def test_kwargs(self, arg1=None, arg2=None):
            urllib.request.urlopen(FILE_URL)

    return TestClass()
