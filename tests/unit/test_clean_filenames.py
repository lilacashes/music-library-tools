__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

import re
from pathlib import Path
from os import listdir
from typing import Callable, Any, List

import pytest

from media_tools.clean_filenames import FilenameCleaner
from media_tools.util import DuplicateStringRemover
from media_tools.util.media_extensions import MEDIA_EXTENSIONS

from .conftest import create_files

NUM_TESTS = 10


def test_longest_common_substring_finds_substring() -> None:
    _longest_common_substr_is(['hello world!', 'can i help you?', 'fucking hell!'], 'hel')


def test_longest_common_substring_if_there_is_none() -> None:
    _longest_common_substr_is(['abc', 'def', 'ghi'], '')


def test_longest_common_substring_with_utf8() -> None:
    _longest_common_substr_is(['ÄÖÜ', 'ÖÜxxxxx'], 'ÖÜ')


def test_clean_filenames_succeeds(cleaner: FilenameCleaner) -> None:
    cleaner.clean_filenames()


@pytest.mark.parametrize('extension', MEDIA_EXTENSIONS)
def test_clean_filenames_removes_longest_common_part(
        extension: str, cleaner: FilenameCleaner, testpath: Path
) -> None:
    create_files(testpath, '{:02d} - blah blah blah - {}.' + extension, NUM_TESTS)
    _perform_and_check_cleaning(cleaner, testpath, lambda s: 'blah blah blah' not in s)


@pytest.mark.parametrize('extension', MEDIA_EXTENSIONS)
def test_clean_filenames_leaves_numbering_scheme_intact(
        extension: str, cleaner: FilenameCleaner, testpath: Path
) -> None:
    create_files(testpath, '{:02d} - blah blah blah - {}.' + extension, NUM_TESTS)
    _perform_and_check_cleaning_with_regex(cleaner, testpath, r'^\d+\s*-?\s*')


@pytest.mark.parametrize('extension', MEDIA_EXTENSIONS)
def test_clean_filenames_leaves_extension_intact(
        extension: str, cleaner: FilenameCleaner, testpath: Path
) -> None:
    create_files(testpath, '{:02d} - {}.' + extension, NUM_TESTS)
    _perform_and_check_cleaning_with_regex(cleaner, testpath, r'^\d+\s*-?\s\w*\.' + extension + '$')


def test_clean_filenames_leaves_long_extension_intact(
        cleaner: FilenameCleaner, testpath: Path
) -> None:
    create_files(testpath, '{:02d} - {}.prettylongextension', NUM_TESTS)
    _perform_and_check_cleaning_with_regex(
        cleaner, testpath, r'^\d+\s*-?\s\w*\.prettylongextension$'
    )


def test_clean_filenames_recognizes_non_extensions(
        cleaner: FilenameCleaner, testpath: Path
) -> None:
    create_files(testpath, '{:02d} - {}.ext with spaces? nah', NUM_TESTS)
    _perform_and_check_cleaning(
        cleaner, testpath,
        lambda s: re.search(s, r'^\d+\s*-?\s\w*\.ext with spaces? nah$') is None
    )


@pytest.mark.parametrize('extension', MEDIA_EXTENSIONS)
def test_clean_filenames_recognizes_period_in_filename(
        extension: str, cleaner: FilenameCleaner, testpath: Path
) -> None:
    create_files(testpath, '{:02d} - {}.www.spam.me.' + extension, NUM_TESTS)
    _perform_and_check_cleaning(cleaner, testpath, lambda s: '.www.spam.me' not in s)
    _perform_and_check_cleaning_with_regex(
        cleaner, testpath, r'^\d+\s*-?\s\w*\.' + extension + '$'
    )


@pytest.mark.parametrize('extension', MEDIA_EXTENSIONS)
def test_clean_filenames_honors_min_length(
        extension: str, cleaner: FilenameCleaner, testpath: Path
) -> None:
    create_files(testpath, '{:02d}-blah-{}.' + extension, NUM_TESTS)
    _perform_and_check_cleaning(cleaner, testpath, lambda s: '-blah-' in s, min_length=7)


@pytest.mark.parametrize('extension', MEDIA_EXTENSIONS)
def test_clean_filenames_recursively(
        extension: str, cleaner: FilenameCleaner, testpath: Path
) -> None:
    subdir = testpath / 'subdir'
    subdir.mkdir()
    create_files(testpath, '{:02d} - blah blah blah - {}.' + extension, NUM_TESTS)
    create_files(testpath, 'subdir/{:02d} - blub blub blub - {}.' + extension, NUM_TESTS)
    _perform_cleaning(cleaner, min_length=4, recurse=True)
    for file in listdir(testpath):
        assert 'blah' not in file
    for file in listdir(subdir):
        assert 'blub' not in file


def _longest_common_substr_is(strings: List[str], expected: str) -> None:
    assert DuplicateStringRemover.longest_common_substring(strings) == expected


def _perform_and_check_cleaning_with_regex(
        cleaner: FilenameCleaner, testpath: Path, regex: str
) -> None:
    _perform_and_check_cleaning(cleaner, testpath, lambda s: re.search(regex, s) is not None)


def _perform_and_check_cleaning(
        cleaner: FilenameCleaner, testpath: Path, assertion: Callable[[str], bool], **kwargs: Any
) -> None:
    _perform_cleaning(cleaner, **kwargs)
    for file in listdir(testpath):
        assert assertion(file)


def _perform_cleaning(cleaner: FilenameCleaner, **kwargs: Any) -> None:
    cleaner.clean_filenames(**kwargs)
