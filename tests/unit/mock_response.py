__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

from pathlib import Path
from typing import Callable, List
from unittest.mock import Mock


def urlopen_file(base_dir: Path, saved_response_file: Path) -> Mock:
    return _urlopen_from_response(_response_from_file(base_dir, saved_response_file))


def urlopen_files(base_dir: Path, saved_response_files: List[Path]) -> Mock:
    return _urlopen_from_responses(
        [_response_from_file(base_dir, file) for file in saved_response_files]
    )


def urlopen_string(response_content: str) -> Mock:
    return _urlopen_from_response(
        _response_from_string(response_content)
    )


def _urlopen_from_response(response: Mock) -> Mock:
    return _mock_urlopen(Mock(return_value=response))


def _urlopen_from_responses(responses: List[Mock]) -> Mock:
    return _mock_urlopen(lambda *args: responses.pop(0))


def _response_from_file(base_dir: Path, saved_response_file) -> Mock:
    with (base_dir / saved_response_file).open() as f:
        return _response_from_string(f.read())


def _mock_urlopen(mock_enter: Callable) -> Mock:
    urlopen = Mock()
    urlopen.__enter__ = mock_enter
    urlopen.__exit__ = Mock()
    return urlopen


def _response_from_string(content) -> Mock:
    response = Mock()
    response.read = Mock(return_value=content.encode('utf-8'))
    return response
