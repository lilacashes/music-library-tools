from dataclasses import dataclass
from pathlib import Path
from typing import Type

import pytest
from pydub import AudioSegment

from media_tools.util.length_reader import (
    LengthReader, MutagenLengthReader, SoundfileLengthReader, AudioreadLengthReader,
    Eyed3LengthReader
)
from .conftest import create_file

__author__ = 'Lene Preuss <lene.preuss@gmail.com>'


@dataclass
class Unsupported:
    reader: Type[LengthReader]
    format: str


UNSUPPORTED_FORMATS = [
    Unsupported(SoundfileLengthReader, 'mp3'),
    Unsupported(Eyed3LengthReader, 'mp3'),
    Unsupported(Eyed3LengthReader, 'flac'),
    Unsupported(Eyed3LengthReader, 'ogg'),
]


@pytest.mark.parametrize('length_seconds', [1, 2])
@pytest.mark.parametrize('file_type', ['mp3', 'flac', 'ogg'])
@pytest.mark.parametrize('reader_class', [
    MutagenLengthReader, SoundfileLengthReader, AudioreadLengthReader, Eyed3LengthReader
])
def test_correct_length_is_read(
        length_seconds: int, file_type: str, reader_class: Type[LengthReader], testpath: Path
) -> None:
    for combination in UNSUPPORTED_FORMATS:
        if reader_class is combination.reader and file_type == combination.format:
            pytest.skip(f'format {file_type} unsupported by {reader_class.__name__}')
            return
    file = create_sound_file(testpath, length_seconds, file_type)
    length_reader = reader_class(file)
    assert int(length_reader.get_duration()) == length_seconds


@pytest.mark.parametrize('file_type', ['mp3', 'flac', 'ogg'])
@pytest.mark.parametrize('reader_class', [
    MutagenLengthReader, SoundfileLengthReader, AudioreadLengthReader, Eyed3LengthReader
])
def test_no_exception_thrown_on_bad_music_file(
        file_type: str, reader_class: Type[LengthReader], testpath: Path
) -> None:
    for combination in UNSUPPORTED_FORMATS:
        if reader_class is combination.reader and file_type == combination.format:
            pytest.skip(f'format {file_type} unsupported by {reader_class.__name__}')
            return
    create_file(testpath / f'test.{file_type}')
    length_reader = reader_class(testpath / f'test.{file_type}')
    assert length_reader.get_duration() is None


def create_sound_file(testpath: Path, length_seconds: int, file_type: str) -> Path:
    audio = AudioSegment.silent(duration=length_seconds * 1000)
    out_path = testpath / f'filename.{file_type}'
    audio.export(out_path, format=file_type)
    return out_path
