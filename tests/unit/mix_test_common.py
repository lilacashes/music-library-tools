__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

import logging
import unittest
from pathlib import Path
from shutil import copytree
from typing import List
from unittest.mock import patch

import requests_mock
from tinytag import TinyTag

from media_tools.util.mixcloud import MixPath, create_mix, Mix, MultiMix
from media_tools.util.mixcloud.constants import DEFAULT_AUDIO_FILE_TYPES, MIXCLOUD_API_UPLOAD_URL

MOCK_ACCESS_TOKEN = 'mock'  # nosec
RESPONSE_OK = dict(status_code=200, json={'result': {'message': 'OK'}})
RESPONSE_RATE_LIMITED = dict(
    status_code=403, json={'error': {'type': 'RateLimitException', 'retry_after': 0.01}}
)
RESPONSE_500 = dict(status_code=500, json={'error': {}})


def upload_with_retries(mix: Mix, should_retry=True, **kwargs) -> None:
    mix.export()
    for retries in (0, 1, 3):
        mocker = _mock_upload(kwargs, mix, retries)
        expected_calls = retries + 1 if should_retry else 1
        assert mocker.call_count == expected_calls


def mock_upload_mix(testpath: Path, **kwargs) -> unittest.mock.Mock:
    create_tags(testpath)
    with patch('logging.info') as mock_info:
        logging.basicConfig(level=logging.INFO)
        mix = create_mix_with_audio(testpath, tagged=True)
        mix.export()
        _mock_upload(kwargs, mix, 0)
    return mock_info


def _mock_upload(kwargs, mix, retries):
    with requests_mock.Mocker() as mocker:
        mocker.post(f'{MIXCLOUD_API_UPLOAD_URL}/?access_token={MOCK_ACCESS_TOKEN}', **kwargs)
        try:
            mix.upload(max_retry=retries)
        except SystemExit:
            pass
        assert mocker.called
    return mocker


def create_empty_mix(testpath: Path, **kwargs):
    mix_path = MixPath(testpath, tuple(f'*.{ext}' for ext in DEFAULT_AUDIO_FILE_TYPES))
    return create_mix(mix_path, MOCK_ACCESS_TOKEN, crossfade_ms=0, **kwargs)


def audio_path(tagged: bool) -> Path:
    audio_folder = f"audio_{'tagged' if tagged else 'untagged'}"
    return Path(__file__).resolve().parent / 'data' / audio_folder


def create_mix_with_audio(testpath: Path, tagged: bool, **kwargs):
    copytree(str(audio_path(tagged)), str(testpath), dirs_exist_ok=True)
    return create_empty_mix(testpath, **kwargs)


def num_audio_files_in_mix(tagged: bool) -> int:
    return len(list(audio_path(tagged).iterdir()))


def create_multimix(testpath: Path, **kwargs):
    return MultiMix(
        MixPath(testpath, tuple(f'*.{ext}' for ext in DEFAULT_AUDIO_FILE_TYPES)),
        MOCK_ACCESS_TOKEN, total_length=4, crossfade_ms=0,
        **kwargs
    )


def create_multimix_with_audio(testpath: Path, tagged: bool, **kwargs):
    audio_folder = f"audio_{'tagged' if tagged else 'untagged'}"
    copytree(
        str(Path(__file__).resolve().parent / 'data' / audio_folder), str(testpath),
        dirs_exist_ok=True
    )
    return MultiMix(
        MixPath(testpath, tuple(f'*.{ext}' for ext in DEFAULT_AUDIO_FILE_TYPES)),
        MOCK_ACCESS_TOKEN, total_length=4, crossfade_ms=0,
        **kwargs
    )


def check_mix_metadata(
        mix: Mix, title: str, tags: List[str], description: str, picture: str = None
):
    assert title in mix.title, f'{title} != {mix.title}'
    assert len(mix.tags) == len(tags), str(mix.tags)
    for tag in tags:
        assert tag in mix.tags
    assert mix.description == description
    if picture is not None:
        assert picture == mix.picture.name


def create_tags(testpath: Path, num_tags: int = 2):
    with (testpath / 'tags.txt').open('w') as tags_file:
        for i in range(num_tags):
            tags_file.write(f'tag{i + 1}\n')


def duration(audio: Path) -> float:
    return TinyTag.get(str(audio)).duration
