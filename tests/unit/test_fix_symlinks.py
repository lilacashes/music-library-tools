from os import listdir
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Generator

import pytest

from media_tools.clean_filenames import FilenameCleaner
from media_tools.util.symlink_fixer import SymlinkFixer
from media_tools.util.numbering_fixer import NumberingFixer

__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

UNDO_DB = 'undo.pickle'


@pytest.fixture
def renamed_file() -> str:
    return '03 - Test C.mp3'  # found in data/audacious_config


@pytest.fixture
def nonexisting_file(testpath: Path, local_path: Path, renamed_file: str) -> str:
    return create_symlink(local_path, testpath, renamed_file)


def create_symlink(from_path: Path, to_path, filename):
    (to_path / filename).symlink_to(from_path / filename)
    return filename


@pytest.fixture
def fixer(testpath) -> SymlinkFixer:
    return SymlinkFixer(testpath)


@pytest.fixture
def db_dir() -> Generator[TemporaryDirectory, None, None]:
    db_dir = TemporaryDirectory()
    yield db_dir
    try:
        db_dir.cleanup()
    except FileNotFoundError:
        pass


def test_find_broken_symlinks(fixer: SymlinkFixer, nonexisting_file: str) -> None:
    links = fixer.find_broken_symlinks()
    assert 1 == len(links)
    assert nonexisting_file in str(links[0])


def test_find_broken_symlinks_with_original_targets(
        fixer: SymlinkFixer, nonexisting_file: str
) -> None:
    links = fixer.find_broken_symlinks_with_original_targets()
    assert 1 == len(links)
    assert nonexisting_file in links[0][0].name
    assert nonexisting_file in links[0][1].name


def test_patterns_to_try_renumbered_file(
        fixer: SymlinkFixer, nonexisting_file: str, local_path: Path, renamed_file: str
) -> None:
    links = fixer.find_broken_symlinks_with_original_targets()
    assert local_path / renamed_file in fixer.patterns_to_try(links[0][1], {})


def test_patterns_to_try_renumbered_file_with_symlink_also_renumbered(
        fixer: SymlinkFixer, nonexisting_file: str, testpath: Path
) -> None:
    numbering_fixer = NumberingFixer(testpath)
    renamed_nonexisting = numbering_fixer._fix_numbering_for_file(
        testpath / nonexisting_file
    ).destination
    links = fixer.find_broken_symlinks_with_original_targets()
    assert testpath / renamed_nonexisting in fixer.patterns_to_try(links[0][0], {})


def test_patterns_to_try_junk_in_filename(
        fixer: SymlinkFixer, nonexisting_file: str, local_path: Path, renamed_file: str
) -> None:
    links = fixer.find_broken_symlinks_with_original_targets()
    assert local_path / renamed_file in fixer.patterns_to_try(links[0][1], {})


def test_fix_broken_symlinks_renumbered_file(
        audacious_config: Path, testpath: Path, renamed_file: str
) -> None:
    link = create_symlink(audacious_config, testpath, '03. Test C.mp3')
    with FilenameCleaner(str(testpath), force=True, undo_db=str(testpath / UNDO_DB)) as cleaner:
        _perform_fix_and_check(link, cleaner, testpath, renamed_file, audacious_config)


def test_fix_broken_symlinks_junk_in_filename(
        audacious_config: Path, testpath: Path, renamed_file: str
) -> None:
    link = create_symlink(audacious_config, testpath, '03_Test_C.mp3')
    with FilenameCleaner(str(testpath), force=True, undo_db=str(testpath / UNDO_DB)) as cleaner:
        _perform_fix_and_check(link, cleaner, testpath, renamed_file, audacious_config)


def test_fix_broken_symlinks_renumbered_file_with_symlink_also_renumbered(
        audacious_config: Path, testpath: Path, renamed_file: str
) -> None:
    nonexisting = create_symlink(audacious_config, testpath, '03. Test C.mp3')
    fixer = NumberingFixer(testpath)
    renamed_nonexisting = fixer._fix_numbering_for_file(testpath / nonexisting)
    with FilenameCleaner(str(testpath), force=True, undo_db=str(testpath / UNDO_DB)) as cleaner:
        _perform_fix_and_check(
            renamed_nonexisting.source, cleaner, testpath, renamed_file, audacious_config
        )


def _perform_fix_and_check(
        nonexisting_file: str, cleaner: FilenameCleaner, testpath: Path, renamed_file: str,
        local_path: Path
) -> None:
    cleaner.fix_symlinks()
    assert not (testpath / nonexisting_file).exists(), \
        f'{nonexisting_file} is in {listdir(testpath)}'
    assert (testpath / renamed_file).exists(), f'{renamed_file} not in {listdir(testpath)}'
    assert (testpath / renamed_file).resolve() == local_path / renamed_file
