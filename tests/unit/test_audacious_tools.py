__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

from pathlib import Path

import pytest

from media_tools.util.audacious_tools import AudaciousTools

ALL_FILES_IN_PLAYLIST = [
    'data/audacious_config/01 - Test A.mp3', 'data/audacious_config/Test B.mp3', 'NONEXISTENT.mp3',
    'data/audacious_config/04. Test D.mp3', 'data/audacious_config/05 Test E.mp3',
    'data/audacious_config/1-01 Test F.mp3'
]
EXISTING_FILES_IN_PLAYLIST = [
    'data/audacious_config/01 - Test A.mp3', 'data/audacious_config/Test B.mp3',
    'data/audacious_config/04. Test D.mp3', 'data/audacious_config/05 Test E.mp3',
    'data/audacious_config/1-01 Test F.mp3'
]


def test_playlist_order(audacious: AudaciousTools) -> None:
    assert 2 == len(audacious.get_playlist_order())
    assert '0001' in audacious.get_playlist_order()


def test_get_currently_playing_playlist_id(audacious: AudaciousTools) -> None:
    # currently playing playlist depends on external audacious state, so this is best we can do
    try:
        audacious.get_currently_playing_playlist_id()
    except FileNotFoundError:
        pytest.skip('audtools not installed')
    except IndexError:
        pytest.skip('audacious playing a different playlist')


@pytest.mark.parametrize('present_file', ALL_FILES_IN_PLAYLIST)
def test_files_in_playlist(present_file: str, audacious: AudaciousTools, local_path: Path) -> None:
    files = audacious.files_in_playlist('0001')
    assert 6 == len(files)
    assert _full_path(local_path, present_file) in files


@pytest.mark.parametrize('present_file', EXISTING_FILES_IN_PLAYLIST)
def test_files_in_playlist_existing_only(
        present_file: str, audacious: AudaciousTools, local_path: Path
) -> None:
    files = audacious.files_in_playlist('0001', existing_only=True)
    assert 5 == len(files)
    assert _full_path(local_path, present_file) in files
    assert _full_path(local_path, 'NONEXISTENT.mp3') not in files


@pytest.mark.parametrize('present_file', ALL_FILES_IN_PLAYLIST)
def test_get_files_to_copy_full_playlist(
        present_file: str, audacious: AudaciousTools, local_path: Path
) -> None:
    files = audacious.get_files_to_copy(0, '0001')
    assert 6 == len(files)
    assert _full_path(local_path, present_file) in files


def test_get_files_to_copy_length_limit(audacious: AudaciousTools, local_path: Path) -> None:
    files = audacious.get_files_to_copy(1, '0001')
    assert 1 == len(files)
    assert _full_path(local_path, ALL_FILES_IN_PLAYLIST[0]) in files


def _full_path(local_path: Path, file: str) -> str:
    return str(local_path / file)
