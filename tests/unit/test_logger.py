__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

import logging
from argparse import Namespace

import coloredlogs

from media_tools.util.logging import setup_logging


def test_log_level_default():
    setup_logging(Namespace())
    assert coloredlogs.get_level() == logging.INFO


def test_log_level_debug():
    setup_logging(Namespace(debug=True))
    assert coloredlogs.get_level() == logging.DEBUG


def test_log_level_quiet():
    setup_logging(Namespace(quiet=True))
    assert coloredlogs.get_level() == logging.WARNING
