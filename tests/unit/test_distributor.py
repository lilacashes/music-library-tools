__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

from pathlib import Path
from unittest.mock import patch

import pytest

from media_tools.util.buying.distributor import Beatport, Amazon
from media_tools.util.buying.track import Track
from tests.buying_common import TRACK, BEATPORT_URL, AMAZON_URL
from tests.unit.mock_response import urlopen_file

BEATPORT_SEARCH_PREFIX = 'search?q='
AMAZON_SEARCH_PREFIX = 'ref=nb_sb_noss?__mk_de_DE=ÅMÅŽÕÑ&url=search-alias=aps&field-keywords='


@pytest.fixture
def base_dir(local_path) -> Path:
    return local_path / 'data'


def test_beatport_search_url():
    assert Beatport().search_url(TRACK) == BEATPORT_URL


def test_beatport_is_present_when_present(base_dir: Path):
    present_track = Track('Steve Lacy', 'Zoo Original Mix')
    urlopen = urlopen_file(base_dir, search_fixture_file(BEATPORT_SEARCH_PREFIX, present_track))
    with patch('urllib.request.urlopen', return_value=urlopen):
        assert Beatport().is_present(present_track)


def test_beatport_is_present_when_not_present(base_dir: Path):
    absent_track = Track('I dont exist on Beatport', 'Nor will I ever')
    urlopen = urlopen_file(base_dir, search_fixture_file(BEATPORT_SEARCH_PREFIX, absent_track))
    with patch('urllib.request.urlopen', return_value=urlopen):
        assert not Beatport().is_present(absent_track)


def test_amazon_search_url():
    assert Amazon().search_url(TRACK) == AMAZON_URL


def test_amazon_is_present_when_present(base_dir: Path):
    present_track = Track('Madonna', 'True Blue')
    urlopen = urlopen_file(base_dir, search_fixture_file(AMAZON_SEARCH_PREFIX, present_track))
    with patch('urllib.request.urlopen', return_value=urlopen):
        assert Amazon().is_present(present_track)


def test_amazon_is_present_when_not_present(base_dir: Path):
    absent_track = Track('I dont exist on Amazon', 'Nor will I ever')
    urlopen = urlopen_file(base_dir, search_fixture_file(AMAZON_SEARCH_PREFIX, absent_track))
    with patch('urllib.request.urlopen', return_value=urlopen):
        assert not Amazon().is_present(absent_track)


def search_fixture_file(search_prefix: str, track: Track) -> Path:
    return Path(f'{search_prefix}"{track.artist}"+"{track.title}"')
