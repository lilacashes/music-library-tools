import re
from pathlib import Path

import pytest

from .conftest import create_files

from media_tools.clean_filenames import FilenameCleaner

__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

NUM_TESTS = 3
CORRECT_STANDARD_PATTERN = r'^\d\d\ - blah blub.mp3$'
CORRECT_LEADING_CD_ID_PATTERN = r'1-\d\d - blah blub.mp3'
STANDARD_PATTERNS = [
    '{:02d} - blah_blub.mp3',     # underscore
    '{:02d} - blah    blub.mp3',  # multiple spaces
    '{:02d} - blah____blub.mp3',  # multiple underscores
    '{:02d} - -blah blub.mp3',    # double dashes
    '{:02d} - - blah blub.mp3',   # double dashes
    '{:02d} -  - blah blub.mp3',  # double dashes
    '{:02d} -- blah blub.mp3',    # double dashes
    '{:02d} – blah blub.mp3',     # em dash
    '{:02d} - blah blub .mp3',    # trailing space
    '{:02d} - blah blub   .mp3',  # trailing spaces
    '{:02d} - blah blub-.mp3',    # trailing dash
    '{:02d} - blah blub--.mp3',   # trailing dashes
    '{:02d} - blah blub -.mp3',   # trailing space then dash
    '{:02d} - blah blub- .mp3',   # trailing dash then space
    '-{:02d} - blah blub.mp3',    # leading dash
    ' {:02d} - blah blub.mp3',    # leading space
    '.{:02d} - blah blub.mp3',    # leading dot
    '{:02d} - blah [] blub.mp3',  # empty brackets
    '{:02d} -, blah blub.mp3',    # dash-comma (does not loop infinitely and gives sensible result)
    '[{:02d}] blah blub.mp3',     # brackets around track no.
    '[{:02d}]-blah blub.mp3',     # brackets around track no. and dash
    '{:02d}]-blah blub.mp3',   # brackets around track no.
    '{:02d} - blah blub-adbfefba.mp3',  # trailing hex string
]
CUSTOM_PATTERNS = [
    ('{:02d} - blah , blub.mp3', r'\d\d - blah, blub.mp3'),  # free comma
    ('1-{:02d} - blah blub.mp3', r'1-\d\d - blah blub.mp3'),  # leading CD id stays untouched
    ('{:02d} - .mp3', r'\d\d.mp3'),  # track number with trailing dash and spaces
    ('{:02d} -.mp3', r'\d\d.mp3'),  # track number with trailing dash and space
    ('{:02d}-.mp3', r'\d\d.mp3'),  # track number with trailing dash
]
LEADING_CD_ID_PATTERNS = [
    '1-{:02d}. blah blub.mp3',     # leading CD id with dot and space
    '1-{:02d}.blah blub.mp3',      # leading CD id with dot, no space
    '1 - {:02d}.blah blub.mp3',    # leading CD id with spaces and dot
    '1 - {:02d}. blah blub.mp3',   # leading CD id with spaces and dot
    '1 - {:02d}-blah blub.mp3',    # leading CD id with spaces and dash
    '1 - {:02d}- blah blub.mp3',   # leading CD id with spaces & dash
    '1 - {:02d} - blah blub.mp3',  # leading CD id with spaces & dash
    '1.{:02d} blah blub.mp3',      # leading CD id with dot inbetween
]


@pytest.mark.parametrize('pattern', STANDARD_PATTERNS)
def test_pattern_is_fixed(pattern: str, cleaner: FilenameCleaner, testpath: Path) -> None:
    create_files(testpath, pattern, NUM_TESTS)
    _perform_and_check_cleaning(cleaner, testpath, CORRECT_STANDARD_PATTERN)


@pytest.mark.parametrize('pattern, regex', CUSTOM_PATTERNS)
def test_pattern_is_fixed_with_result(
        pattern: str, regex: str, cleaner: FilenameCleaner, testpath: Path
) -> None:
    create_files(testpath, pattern, NUM_TESTS)
    _perform_and_check_cleaning(cleaner, testpath, regex)


@pytest.mark.parametrize('pattern', LEADING_CD_ID_PATTERNS)
def test_pattern_is_fixed_with_leading_cd_id(
        pattern: str, cleaner: FilenameCleaner, testpath: Path
) -> None:
    create_files(testpath, pattern, NUM_TESTS)
    _perform_and_check_cleaning(cleaner, testpath, CORRECT_LEADING_CD_ID_PATTERN)


@pytest.mark.parametrize(
    'pattern',
    STANDARD_PATTERNS + LEADING_CD_ID_PATTERNS + [pattern[0] for pattern in CUSTOM_PATTERNS]
)
def test_no_stray_stdout(
        capsys: pytest.CaptureFixture, pattern: str, cleaner: FilenameCleaner, testpath: Path
) -> None:
    create_files(testpath, pattern, NUM_TESTS)
    captured = capsys.readouterr()
    cleaner.clean_junk()
    assert captured.out == ''


def _perform_and_check_cleaning(
        cleaner: FilenameCleaner, testpath: Path, regex: str
) -> None:
    cleaner.clean_junk()
    cleaner.clean_numbering()
    for file in testpath.iterdir():
        assert re.search(regex, file.name), f'Found {[f.name for f in testpath.iterdir()]}'
