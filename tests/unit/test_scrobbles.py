__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

import json
import pickle
import sys
from bz2 import open as bzopen
from gzip import open as gzopen
from lzma import open as xzopen
from pathlib import Path
from typing import Any, Callable, Dict, Optional
from unittest.mock import Mock, patch

import pytest
from pylast import PyLastError

from media_tools.util.scrobbles import get_output_filename, LastFMUser, Scrobbles, year_range

MOCK_DATA: Dict[str, Any] = {}


def dump_path(format: str, compression: Optional[str] = None) -> Path:
    return Path(f'mock.{format}' if compression is None else f'mock.{format}.{compression}')


@pytest.mark.parametrize('dump_format', ['json', 'pickle'])
@pytest.mark.parametrize('compression', [None, 'gz', 'bz2', 'xz'])
@pytest.mark.skipif(sys.version_info < (3, 8), reason='missing_ok parameter introduced in 3.8')
@patch('media_tools.util.scrobbles.Scrobbles.get_data', return_value=MOCK_DATA)
def test_dump_created(_: Mock, dump_format, compression) -> None:
    dump_file = dump_path(dump_format, compression)
    check_creation(dump_file)
    dump_file.unlink(missing_ok=True)


@pytest.mark.parametrize('dump_format,load', [('json', json.load), ('pickle', pickle.load)])
@pytest.mark.parametrize(
    'compression, open_function', [(None, open), ('gz', gzopen), ('bz2', bzopen), ('xz', xzopen)]
)
@pytest.mark.skipif(sys.version_info < (3, 8), reason='missing_ok parameter introduced in 3.8')
@patch('media_tools.util.scrobbles.Scrobbles.get_data', return_value=MOCK_DATA)
def test_dump_file_contains_correct_format(
        _: Mock, dump_format, load, compression, open_function
) -> None:
    dump_file = dump_path(dump_format, compression)
    check_data_format(dump_file, open_function, load)
    dump_file.unlink(missing_ok=True)


def test_get_output_filename():
    assert get_output_filename(None, None) == 'backup_lastfm_all.pickle.bz2'
    assert get_output_filename(1999, None) == 'backup_lastfm_1999.pickle.bz2'
    assert get_output_filename(None, 'test') == 'test'
    assert get_output_filename(1999, 'test') == 'test'


def test_get_data_calls_all_required_functions_if_requested():
    user = Mock()
    Scrobbles(LastFMUser(user), None, 10, 0, True).get_data()
    user.get_playcount.assert_called_once()
    user.get_recent_tracks.assert_called_once_with(limit=10, **year_range(None))
    user.get_loved_tracks.assert_called_once()
    user.get_top_tracks.assert_called_once()
    user.get_top_artists.assert_called_once()
    user.get_top_albums.assert_called_once()


def test_get_data_does_not_calls_all_required_functions_if_not_requested():
    user = Mock()
    Scrobbles(LastFMUser(user), None, 10, 0, False).get_data()
    user.get_playcount.assert_called_once()
    user.get_recent_tracks.assert_called_once_with(limit=10, **year_range(None))
    user.get_loved_tracks.assert_not_called()
    user.get_top_tracks.assert_not_called()
    user.get_top_artists.assert_not_called()
    user.get_top_albums.assert_not_called()


def test_get_data_returns_all_fields():
    user = Mock()
    result = Scrobbles(user, None, 10, 0, False).get_data()
    assert 'time' in result
    assert 'play_count' in result
    assert 'tracks' in result
    assert 'loved' in result
    assert 'top_tracks' in result
    assert 'top_artists' in result
    assert 'top_albums' in result
    assert 'top_tags' in result


@patch('media_tools.util.scrobbles.Scrobbles.get_and_wait', side_effect=PyLastError)
@patch('logging.debug')
@patch('logging.info')
@patch('logging.warning')
@patch('logging.error')
def test_get_data_with_exception(error, warning, info, debug, get_and_wait):
    check_logging_with_retries(1, warning, info, debug)
    assert error.call_count == 1


@patch('media_tools.util.scrobbles.Scrobbles.get_and_wait', side_effect=PyLastError)
@patch('logging.debug')
@patch('logging.info')
@patch('logging.warning')
@patch('logging.error')
def test_get_data_with_exception_and_retry(error, warning, info, debug, get_and_wait):
    check_logging_with_retries(1, warning, info, debug)
    assert error.call_count == 1


@patch('media_tools.util.scrobbles.Scrobbles.get_and_wait', side_effect=PyLastError)
@patch('logging.debug')
@patch('logging.info')
@patch('logging.warning')
@patch('logging.error')
def test_get_data_with_exception_and_more_retries(error, warning, info, debug, get_and_wait):
    check_logging_with_retries(2, warning, info, debug)
    assert error.call_count == 1


def check_logging_with_retries(retries: int, warning: Mock, info: Mock, debug: Mock) -> None:
    user = Mock()
    with pytest.raises(PyLastError):
        Scrobbles(
            user, year=None, limit=10, max_retry_wait=retries, all_data=False
        ).get_data(wait_time=0)
    assert warning.call_count == retries
    assert debug.call_count == retries + 1
    assert info.call_count == retries + 1


def check_creation(path: Path) -> None:
    Scrobbles(LastFMUser(None), None, 10, 0, False).get_and_dump(path.name)
    assert path.exists()


def check_data_format(path: Path, open_function: Callable, load: Callable) -> None:
    Scrobbles(LastFMUser(None), None, 10, 0, False).get_and_dump(path.name)
    with open_function(path, 'rb') as file:
        data = load(file)
    assert data == MOCK_DATA
