__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

import logging
from hashlib import md5
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Generator, List

import coloredlogs
import pytest

from media_tools.clean_filenames import FilenameCleaner
from media_tools.util import find_dirs
from media_tools.util.audacious_tools import AudaciousTools
from media_tools.util.playlist_copier import PlaylistCopier
from tests.unit.mix_test_common import num_audio_files_in_mix

UNDO_DB = 'undo.pickle'


def create_file(name: Path) -> None:
    open(name, 'a').close()


def create_files(testpath: Path, template: str, number: int) -> List[Path]:
    return [create_file_and_path(i, template, testpath) for i in range(1, number + 1)]


def create_file_and_path(i: int, template: str, testpath: Path) -> Path:
    path = testpath / template.format(i, unique_string(i))
    create_file(path)
    return path


def unique_string(i: int) -> str:
    return md5(str(i).encode('utf-8'), usedforsecurity=False).hexdigest()


@pytest.fixture
def local_path() -> Path:
    return Path(__file__).resolve().parent


@pytest.fixture
def testdir() -> Generator[TemporaryDirectory, None, None]:
    coloredlogs.set_level(level=logging.CRITICAL)
    testdir = TemporaryDirectory()
    yield testdir
    try:
        testdir.cleanup()
    except FileNotFoundError:
        pass


@pytest.fixture
def testpath(testdir: TemporaryDirectory) -> Generator[Path, None, None]:
    yield Path(testdir.name)


@pytest.fixture
def audacious_config() -> Path:
    candidates = find_dirs(Path('.'), lambda d: str(d).endswith('data/audacious_config'))
    if len(candidates) != 1:
        raise ValueError(
            f'Test audacious config dir not found in {Path.cwd()} - see {candidates}'
        )
    return candidates[0].resolve()


@pytest.fixture
def audacious(audacious_config: Path, local_path: Path) -> AudaciousTools:
    return AudaciousTools(str(audacious_config), file_base_dir=str(local_path))


@pytest.fixture
def copier(audacious: AudaciousTools) -> PlaylistCopier:
    return PlaylistCopier(audacious, '0001')


@pytest.fixture
def cleaner(testpath: Path) -> FilenameCleaner:
    return FilenameCleaner(
        str(testpath), force=True, undo_db=str(testpath / UNDO_DB)
    )


@pytest.fixture
def num_audio_files() -> int:
    return num_audio_files_in_mix(tagged=True)
