from pathlib import Path

import pytest

from .conftest import create_file

from media_tools.copy_from_playlist import copy_newest_files

__author__ = 'Lene Preuss <lene.preuss@gmail.com>'


@pytest.fixture
def src_dir(testpath: Path) -> Path:
    src_dir = testpath / 'src'
    (src_dir / 'sub_dir').mkdir(parents=True)
    create_file(src_dir / 'x')
    create_file(src_dir / 'sub_dir' / 'x')
    return src_dir


@pytest.fixture
def dest_dir(testpath: Path) -> Path:
    dest_dir = testpath / 'dest'
    dest_dir.mkdir()
    return dest_dir


def test_files_and_dir_structure_are_copied(src_dir: Path, dest_dir: Path) -> None:
    copy_newest_files(str(src_dir), str(dest_dir), 1)
    assert dest_dir.joinpath('x').is_file()
    assert dest_dir.joinpath('sub_dir').is_dir()
    assert dest_dir.joinpath('sub_dir', 'x').is_file()
