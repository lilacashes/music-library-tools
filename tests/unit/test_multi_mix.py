__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

from pathlib import Path
from unittest.mock import Mock, patch

import pytest

from media_tools.util.mixcloud import MultiMix
from tests.unit.mix_test_common import (
    RESPONSE_OK, create_multimix, create_multimix_with_audio, duration, upload_with_retries
)


@pytest.fixture
def multi_mix(testpath: Path) -> MultiMix:
    return create_multimix(testpath)


def test_create_multimix(multi_mix: MultiMix):
    assert 1 == len(multi_mix.parts)


def test_create_multimix_with_audio(testpath: Path):
    multi_mix = create_multimix_with_audio(testpath, tagged=True)
    assert 1 == len(multi_mix.parts)


def test_multimix_export_runs(testpath: Path, multi_mix: MultiMix) -> None:
    multi_mix.export()
    assert (testpath / 'mix_1.mp3').exists(), list(testpath.glob('**/*'))
    assert duration(testpath / 'mix_1.mp3') < 1


def test_multimix_export_with_audio(testpath: Path, num_audio_files: int) -> None:
    multi_mix = create_multimix_with_audio(testpath, tagged=True)
    multi_mix.export()
    assert (testpath / 'mix_1.mp3').exists(), list(testpath.glob('**/*'))
    assert duration(testpath / 'mix_1.mp3') >= num_audio_files


@patch('logging.info')
def test_re_import_does_not_regenerate_mix(
        mock_info: Mock, testpath: Path, num_audio_files: int
) -> None:
    multi_mix = create_multimix_with_audio(testpath, tagged=True)
    assert mock_info.call_count == num_audio_files + 1
    multi_mix.export()

    mock_info.reset_mock()

    create_multimix_with_audio(testpath, tagged=True)
    print(mock_info.call_args_list)
    assert mock_info.call_count == 2
    mock_info.assert_any_call('MultiMix: %s', testpath.name + ' Part 1')
    mock_info.assert_any_call('Reading present %s', testpath / 'mix_1.mp3')


def test_re_imported_mix_is_tagged_correctly(testpath: Path) -> None:
    multi_mix = create_multimix_with_audio(testpath, tagged=True)
    track_info = multi_mix.parts[0]._track_info
    multi_mix.export()
    mix = create_multimix_with_audio(testpath, tagged=True)
    assert mix.parts[0]._track_info == track_info


@patch('logging.info')
def test_create_multimix_with_verbose(mock_info: Mock, testpath: Path):
    create_multimix(testpath)
    mock_info.assert_called_once_with('MultiMix: %s', testpath.name + ' Part 1')


def test_multimix_upload_success(testpath: Path, multi_mix: MultiMix) -> None:
    upload_with_retries(multi_mix, should_retry=False, **RESPONSE_OK)
