from os import listdir
from pathlib import Path

from media_tools.clean_filenames import FilenameCleaner
from .conftest import create_files


def test_undo_after_clean_filenames(cleaner: FilenameCleaner, testpath: Path) -> None:
    create_files(testpath, '{:02d} - blah blah blah - {}.mp3', 10)
    cleaner.clean_filenames(min_length=5)
    for file in listdir(testpath):
        assert 'blah blah blah' not in file
    cleaner.undo()
    for file in listdir(testpath):
        assert 'blah blah blah' in file
