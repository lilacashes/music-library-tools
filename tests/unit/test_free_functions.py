__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

from itertools import chain
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Tuple

import pytest

from .conftest import create_file
from media_tools.util.audacious_tools import find_first_dir, existing_files
from media_tools.util.playlist_copier import strip_leading_numbers, renumber_file

FILENAMES_TO_CLEAN = [
    '01 - filename.mp3', '01. filename.mp3', '01 filename.mp3', '01filename.mp3',
    '01-filename.mp3', '01- filename.mp3', '01.filename.mp3',
    '1. filename.mp3', '1.filename.mp3', '1 - filename.mp3', '1-filename.mp3', '1 filename.mp3',
    '01--filename.mp3', '001 - filename.mp3', '123 - filename.mp3', '12345678 - filename.mp3',
    '1-01 - filename.mp3', '1-01. filename.mp3', '1-01-filename.mp3', '1-01.filename.mp3',
    'filename.mp3'
]


@pytest.fixture
def file_names(testpath: Path) -> Tuple[str, ...]:
    file_names = ('A', 'B', 'D')
    for file in file_names:
        create_file(testpath.joinpath(file))
    return file_names


@pytest.fixture
def dir_names(testpath: Path, file_names: Tuple[str, ...]) -> Tuple[str, ...]:
    dir_names = ('C', 'E')
    for dir in dir_names:
        testpath.joinpath(dir).mkdir()
        for file in file_names:
            create_file(testpath.joinpath(dir, file))
    return dir_names


def test_find_first_dir_finds_dirs(testdir: TemporaryDirectory, dir_names: Tuple[str, ...]) -> None:
    for dir in dir_names:
        assert find_first_dir(dir, testdir.name) is not None
        assert dir in find_first_dir(dir, testdir.name)


def test_find_first_dir_finds_no_files(
        testdir: TemporaryDirectory, file_names: Tuple[str, ...]
) -> None:
    for file in file_names:
        assert find_first_dir(file, testdir.name) is None


def test_existing_files(testpath: Path, file_names: Tuple[str, ...]) -> None:
    files_to_test = [str(testpath.joinpath(f)) for f in file_names]
    assert files_to_test == existing_files(files_to_test)


def test_existing_files_no_dirs(
        testpath: Path, file_names: Tuple[str, ...], dir_names: Tuple[str, ...]
) -> None:
    files_to_test = [
        str(testpath.joinpath(f)) for f in chain(file_names, dir_names)
    ]
    found_files = existing_files(files_to_test)
    assert files_to_test != found_files
    assert set(found_files).issubset(files_to_test)
    assert len(found_files) > 0


@pytest.mark.parametrize('filename', FILENAMES_TO_CLEAN)
def test_strip_leading_numbers(filename: str):
    assert strip_leading_numbers(filename) == 'filename.mp3'


@pytest.mark.parametrize('total', [9, 99])
@pytest.mark.parametrize('filename', FILENAMES_TO_CLEAN)
def test_renumber_file(total: int, filename: str):
    assert renumber_file(filename, 1, total) == '01 - filename.mp3'


@pytest.mark.parametrize('filename', FILENAMES_TO_CLEAN)
def test_renumber_file_long_list(filename: str):
    assert renumber_file(filename, 1, 100) == '001 - filename.mp3'


@pytest.mark.parametrize('filename', ['1x. blah', 'x1.blah', '² - blah', '1/blah'])
def test_strip_leading_numbers_nonsense(filename: str) -> None:
    assert 'blah' != strip_leading_numbers(filename)
