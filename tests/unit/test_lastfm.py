__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

from json import dumps
from datetime import datetime
from pathlib import Path
from typing import List, Dict
from unittest.mock import Mock, patch

import pytest

from media_tools.util.buying.last_fm import LastFM
from media_tools.util.buying.track import Track
from tests.unit.mock_response import urlopen_file, urlopen_files, urlopen_string


Track.setup(tuple(), None)


def _top_tracks_json(num_tracks: int) -> str:
    return _get_json(
        [Track(f'a_{i}', f't_{i}', play_count=i) for i in range(1, num_tracks + 1)],
        'toptracks'
    )


def _top_tracks(num_tracks: int) -> Mock:
    return urlopen_string(_top_tracks_json(num_tracks))


def _recent_tracks_json(num_tracks: int) -> str:
    return _get_json(
        sum(
            [[Track(f'a_{i + 1}', f't_{i + 1}')] * (num_tracks - i) for i in range(num_tracks)],
            []
        ),
        'recenttracks', artist_field='#text'
    )


def _recent_tracks(num_tracks: int) -> Mock:
    return urlopen_string(_recent_tracks_json(num_tracks))


def _get_json(tracks: List[Track], kind: str, artist_field: str = 'name') -> str:
    return dumps({
        kind: {
            'track': [
                _get_track_dict(track, artist_field, rank + 1) for rank, track in enumerate(tracks)
            ],
            '@attr': {'totalPages': 1, 'page': 1}
        }
    })


def _get_track_dict(track: Track, artist_field: str, rank: int = 1) -> Dict:
    return {
        'name': track.title, 'playcount': track.play_count,
        'artist': {artist_field: track.artist},
        '@attr': {'rank': rank}
    }


def lastfm_fixture_file(limit: int, page: int) -> Path:
    return Path('data') / f'index.html?user=XXXX&api_key=XXXX&format=json&' \
                          f'method=user.getrecenttracks&from=XXXX&to=XXXX&limit={limit}&page={page}'


@pytest.fixture
def base_dir(local_path) -> Path:
    return local_path / 'data'


@pytest.fixture
def api() -> LastFM:
    return LastFM(None, None)


@patch('urllib.request.urlopen', return_value=_top_tracks(1))
def test_top_tracks_basic(urlopen, api: LastFM):
    tracks = api.get_top_tracks()
    _contains_n_tracks(tracks, 1)


@patch('urllib.request.urlopen', return_value=_top_tracks(10))
def test_top_tracks_length(urlopen, api: LastFM):
    tracks = api.get_top_tracks()
    assert 10 == len(tracks)


@patch('urllib.request.urlopen', return_value=_recent_tracks(1))
def test_recent_tracks_basic(urlopen, api: LastFM):
    tracks = api.get_tracks_by_period(datetime.now())
    _contains_n_tracks(tracks, 1)


@patch('urllib.request.urlopen', return_value=_recent_tracks(3))
def test_recent_tracks_sorted_by_count(urlopen, api: LastFM):
    tracks = api.get_tracks_by_period(datetime.now())
    _contains_n_tracks(tracks, 3)


def test_response_fixture(api: LastFM, local_path: Path):
    urlopen = urlopen_file(local_path, lastfm_fixture_file(30, 1))
    with patch('urllib.request.urlopen', return_value=urlopen):
        tracks = api.get_tracks_by_period(datetime.now())
    assert 28 == len(tracks)
    assert 'The+Red+Army+Choir' == tracks[0].artist


def test_multipage_response_fixture(api: LastFM, local_path: Path):
    urlopen = urlopen_files(local_path, [lastfm_fixture_file(10, i) for i in range(1, 4)])
    with patch('urllib.request.urlopen', return_value=urlopen):
        tracks = api.get_tracks_by_period(datetime.now())
    assert 28 == len(tracks)
    assert 'The+Red+Army+Choir' == tracks[0].artist


def test_multipage_response_fixture_respects_limit(api: LastFM, local_path: Path):
    urlopen = urlopen_files(local_path, [lastfm_fixture_file(10, i) for i in range(1, 4)])
    with patch('urllib.request.urlopen', return_value=urlopen):
        tracks = api.get_tracks_by_period(datetime.now(), paging=LastFM.Paging(limit=5))
    assert 5 == len(tracks)
    assert 'The+Red+Army+Choir' == tracks[0].artist


@patch('urllib.request.urlopen', return_value=_top_tracks(10))
def test_top_tracks_min_plays(urlopen, api: LastFM):
    tracks = api.get_top_tracks(paging=LastFM.Paging(min_plays=5))
    assert 6 == len(tracks)
    for i in range(5, 11):
        assert Track(f'a_{i}', f't_{i}', i) in tracks


@patch('urllib.request.urlopen', return_value=_recent_tracks(3))
def test_tracks_by_period_min_plays(urlopen, api: LastFM):
    tracks = api.get_tracks_by_period(datetime.now(), paging=LastFM.Paging(min_plays=2))
    _contains_n_tracks(tracks, 2)


@patch('urllib.request.urlopen', return_value=_recent_tracks(3))
def test_recent_tracks_sets_playcount(urlopen, api: LastFM):
    tracks = api.get_tracks_by_period(datetime.now())
    assert 3 == tracks[0].play_count
    assert 2 == tracks[1].play_count
    assert 1 == tracks[2].play_count


@patch('urllib.request.urlopen', return_value=_top_tracks(10))
def test_top_tracks_respects_limit(urlopen, api: LastFM):
    tracks = api.get_top_tracks(paging=LastFM.Paging(limit=5))
    _contains_n_tracks(tracks, 5)


@patch('urllib.request.urlopen', return_value=_top_tracks(10))
def test_top_tracks_page_1(urlopen, api: LastFM):
    tracks = api.get_top_tracks(paging=LastFM.Paging(limit=5, page=1))
    _contains_n_tracks(tracks, 5)


@patch('urllib.request.urlopen', return_value=_top_tracks(10))
def test_top_tracks_page_2(urlopen, api: LastFM):
    tracks = api.get_top_tracks(paging=LastFM.Paging(limit=5, page=2))
    assert 5 == len(tracks)
    for i in range(6, 11):
        assert Track(f'a_{i}', f't_{i}', i) in tracks


@patch('urllib.request.urlopen', return_value=_top_tracks(10))
def test_top_tracks_page_3(urlopen, api: LastFM):
    tracks = api.get_top_tracks(paging=LastFM.Paging(limit=5, page=3))
    assert 0 == len(tracks)


@patch('urllib.request.urlopen', return_value=_recent_tracks(10))
def test_recent_tracks_respects_limit(urlopen, api: LastFM):
    tracks = api.get_tracks_by_period(datetime.now(), paging=LastFM.Paging(limit=5))
    _contains_n_tracks(tracks, 5)


@patch('urllib.request.urlopen', return_value=_recent_tracks(10))
def test_recent_tracks_page_1(urlopen, api: LastFM):
    tracks = api.get_tracks_by_period(
        datetime.now(), paging=LastFM.Paging(limit=5, page=1)
    )
    _contains_n_tracks(tracks, 5)


@patch('urllib.request.urlopen', return_value=_recent_tracks(10))
def test_recent_tracks_page_2(urlopen, api: LastFM):
    tracks = api.get_tracks_by_period(
        datetime.now(), paging=LastFM.Paging(limit=5, page=2)
    )
    assert 5 == len(tracks)
    for i in range(6, 11):
        assert Track(f'a_{i}', f't_{i}', i) in tracks


@patch('urllib.request.urlopen', return_value=_recent_tracks(10))
def test_recent_tracks_page_3(urlopen, api):
    tracks = api.get_tracks_by_period(
        datetime.now(), paging=LastFM.Paging(limit=5, page=3)
    )
    assert 0 == len(tracks)


def _contains_n_tracks(
        tracks: List[Track], n: int, title_prefix: str = 't', artist_prefix: str = 'a'
) -> None:
    assert n == len(tracks)
    for i, track in enumerate(tracks):
        assert isinstance(track, Track)
        assert f'{title_prefix}_{i + 1}' == track.title
        assert f'{artist_prefix}_{i + 1}' == track.artist
