__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

import sys
import threading
from argparse import ArgumentError
from filecmp import cmp
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import List
from unittest.mock import Mock, patch

import pytest

from media_tools.mixcloud_upload import create_authentication, parse_commandline, prep_mix_dir
from media_tools.util.mixcloud.create_api_token import Help


def data_path():
    return Path(__file__).resolve().parent / 'data' / 'mixcloud'


@pytest.fixture
def data_dir() -> Path:
    return data_path()


@pytest.fixture
def mix_dir() -> TemporaryDirectory:
    return TemporaryDirectory()


@pytest.mark.parametrize('subparser', ['upload', 'create-auth'])
def test_parse_missing_args(subparser):
    with pytest.raises(SystemExit):
        opts = parse_commandline([subparser])
        opts.execute(opts)


@patch('builtins.print')
def test_help_text_is_printed(mock_print: Mock):
    with pytest.raises(SystemExit):
        opts = parse_commandline(['create-auth'])
        opts.execute(opts)
        mock_print.assert_called_with(Help.help_text)


def test_parse_description():
    opts = parse_commandline(['upload', '-d', '.', '-t', 'test', '--description', 'test'])
    assert opts.description == 'test'


@pytest.mark.parametrize(
    'option,value,attribute', [
        ('--description-file', 'description.txt', 'description_file'),
        ('--tags-file', 'tags.txt', 'tags_file')
    ]
)
def test_parse_files(data_dir, option, value, attribute):
    opts = parse_commandline(['upload', '-d', '.', '-t', 'test', option, str(data_dir / value)])
    assert getattr(opts, attribute) == data_dir / value


def test_parse_multiple_tags():
    opts = parse_commandline(['upload', '-d', '.', '-t', 'test', '--tags', 'test1', 'test2'])
    assert opts.tags == ['test1', 'test2']


@pytest.mark.skipif(sys.version_info < (3, 9), reason='exit_on_error parameter introduced in 3.9')
@pytest.mark.parametrize(
    'option', ['--description-file', '--tags-file', '--picture-file', '--auth-token-file']
)
def test_parse_nonexistent_files(option):
    with pytest.raises(ArgumentError):
        parse_commandline(['upload', '-d', '.', '-t', 'test', option, 'nonexistent file'])


@pytest.mark.skipif(sys.version_info < (3, 9), reason='exit_on_error parameter introduced in 3.9')
@pytest.mark.parametrize(
    'option', ['--description-file', '--tags-file', '--picture-file', '--auth-token-file']
)
def test_parse_file_is_dir(option):
    with pytest.raises(ArgumentError):
        parse_commandline(['upload', '-d', '.', '-t', 'test', option, '.'])


@pytest.mark.skipif(sys.version_info < (3, 9), reason='exit_on_error parameter introduced in 3.9')
def test_parse_nonexistent_dir():
    with pytest.raises(ArgumentError):
        parse_commandline(['upload', '-d', 'nonexistent directory', '-t', 'test'])


@pytest.mark.skipif(sys.version_info < (3, 9), reason='exit_on_error parameter introduced in 3.9')
def test_parse_dir_is_file():
    with pytest.raises(ArgumentError):
        parse_commandline(['upload', '-d', __file__, '-t', 'test'])


@pytest.mark.skipif(sys.version_info < (3, 9), reason='exit_on_error parameter introduced in 3.9')
@pytest.mark.parametrize(
    'option1,option2', [
        ('--description-file', '--description'),
        ('--tags-file', '--tags'),
        ('--auth-token-file', '--auth-token-string')
    ]
)
def test_parse_argument_conflicts(option1, option2, local_path):
    with pytest.raises(ArgumentError):
        parse_commandline([
            'upload', '-d', '.', '-t', 'test',
            option1, str(local_path / 'data' / 'mixcloud' / 'empty.txt'),
            option2, 'test'
        ])


@patch('webbrowser.open_new_tab')
def test_create_auth_step_1_success(mock_open_new_tab: Mock):
    opts = parse_commandline(['create-auth', '--create-app'])
    opts.execute(opts)
    mock_open_new_tab.assert_called_once()


@patch('webbrowser.open_new_tab')
@patch.object(threading.Thread, 'start')
@patch.object(threading.Thread, 'join')
def test_create_auth_step_2_success(mock_join: Mock, mock_start: Mock, mock_open_new_tab: Mock):
    opts = parse_commandline(['create-auth', '--create-code', '--client-id', 'XXX'])
    opts.execute(opts)
    mock_start.assert_called_once()
    mock_join.assert_called_once()
    mock_open_new_tab.assert_called_once()


def test_create_auth_step_2_fails_without_client_id():
    with pytest.raises(ArgumentError):
        opts = parse_commandline(['create-auth', '--create-code'])
        opts.execute(opts)


def test_create_authentication_is_equivalent_to_execute():
    with pytest.raises(ArgumentError):
        opts = parse_commandline(['create-auth', '--create-code'])
        create_authentication(opts)


@pytest.mark.parametrize('args', [
    [], ['--client-id', 'XXX'], ['--client-secret', 'YYY'], ['--oauth-code', 'ZZZ'],
    ['--client-id', 'XXX', '--client-secret', 'YYY'], ['--client-id', 'XXX', '--oauth-code', 'ZZZ'],
    ['--client-secret', 'YYY', '--oauth-code', 'ZZZ']
])
def test_create_auth_step_3_fails_without_all_necessary_args(args: List[str]):
    with pytest.raises(ArgumentError):
        opts = parse_commandline(['create-auth', '--create-token'] + args)
        create_authentication(opts)


@patch('webbrowser.open_new_tab')
@patch.object(threading.Thread, 'start')
@patch.object(threading.Thread, 'join')
def test_create_auth_step_3_success(mock_join: Mock, mock_start: Mock, mock_open_new_tab: Mock):
    opts = parse_commandline([
        'create-auth', '--create-token', '--client-id', 'XXX', '--client-secret', 'YYY',
        '--oauth-code', 'ZZZ'
    ])
    opts.execute(opts)
    mock_start.assert_called_once()
    mock_join.assert_called_once()
    mock_open_new_tab.assert_called_once()


@pytest.mark.skipif(sys.version_info < (3, 9), reason='exit_on_error parameter introduced in 3.9')
def test_create_auth_fails_with_conflicting_args():
    with pytest.raises(ArgumentError):
        parse_commandline(['create-auth', '--create-app', '--create-token', '--client-id', 'XXX'])


def test_create_auth_selenium_not_yet_implemented():
    with pytest.raises(NotImplementedError):
        opts = parse_commandline(['create-auth', '--create-app', '--browser', 'selenium'])
        create_authentication(opts)


@pytest.mark.parametrize(
    'option,value', [
        ('--description-file', 'description.txt'),
        ('--tags-file', 'tags.txt'),
        ('--picture-file', '1x1.jpg'),
        ('--picture-file', '1x1.png')
    ]
)
def test_prep_mix_dir_files(mix_dir, data_dir, option, value):
    opts = parse_commandline([
        'upload', '-d', mix_dir.name, '-t', 'test', option, str(data_dir / value)
    ])
    prep_mix_dir(opts)
    assert (Path(mix_dir.name) / value).exists()
    assert cmp(data_dir / value, Path(mix_dir.name) / value)


@pytest.mark.parametrize(
    'option,value,expected', [
        ('--description-file', 'different_description.txt', 'description.txt'),
        ('--tags-file', 'different_tags.txt', 'tags.txt')
    ]
)
def test_prep_mix_dir_files_with_different_names(mix_dir, data_dir, option, value, expected):
    opts = parse_commandline([
        'upload', '-d', mix_dir.name, '-t', 'test', option, str(data_dir / value)
    ])
    prep_mix_dir(opts)
    assert (Path(mix_dir.name) / expected).exists()
    assert cmp(data_dir / value, Path(mix_dir.name) / expected)


@pytest.mark.parametrize(
    'option,value,filename', [
        ('--description', 'test description', 'description.txt'),
        ('--tags', 'tag1\ntag2', 'tags.txt'),
    ]
)
def test_prep_mix_dir_values(mix_dir, data_dir, option, value, filename):
    opts = parse_commandline(['upload', '-d', mix_dir.name, '-t', 'test', option, value])
    prep_mix_dir(opts)
    assert (Path(mix_dir.name) / filename).exists()
    with (Path(mix_dir.name) / filename).open() as file:
        assert file.read() == value


@pytest.mark.parametrize(
    'description_option', [
        ['--description', 'test description'],
        ['--description-file', str(data_path() / 'description.txt')],
        []
    ]
)
@pytest.mark.parametrize(
    'tags_option', [
        ['--tags', 'tag1\ntag2'], ['--tags-file', str(data_path() / 'tags.txt')], []
    ]
)
@pytest.mark.parametrize(
    'picture_option', [['--picture-file', str(data_path() / '1x1.jpg')], []]
)
def test_files_missing_strict(mix_dir, description_option, tags_option, picture_option):
    if description_option and tags_option and picture_option:
        return
    opts = parse_commandline(
        ['upload', '-d', mix_dir.name, '--strict'] +
        description_option + tags_option + picture_option
    )
    with pytest.raises(ValueError):
        prep_mix_dir(opts)


@pytest.mark.parametrize('description_file', ['description.txt', 'different_description.txt'])
@pytest.mark.parametrize('tags_file', ['tags.txt', 'different_tags.txt'])
@pytest.mark.parametrize('picture_file', ['1x1.png', '1x1.jpg'])
def test_files_present_strict(mix_dir, data_dir, description_file, tags_file, picture_file):
    opts = parse_commandline([
        'upload', '-d', mix_dir.name, '-t', 'test', '--strict',
        '--description-file', str(data_dir / description_file),
        '--tags-file', str(data_dir / tags_file),
        '--picture-file', str(data_dir / picture_file)
    ])
    prep_mix_dir(opts)
