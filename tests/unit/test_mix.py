__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

import shutil
import sys
from shutil import copytree
from unittest.mock import ANY, Mock, call, patch, mock_open
from pathlib import Path

import pytest
import requests
from audioread import audio_open

from media_tools.util.mixcloud import AuthorizationError, Mix, get_access_token
from media_tools.util.mixcloud.constants import ACCESS_TOKEN_FILE, ALLOWED_TRACKS_PER_ARTIST, \
    MP3_KBIT_RATE
from tests.unit.mix_test_common import (
    MOCK_ACCESS_TOKEN, RESPONSE_500, RESPONSE_OK, RESPONSE_RATE_LIMITED, audio_path,
    check_mix_metadata,
    create_empty_mix, create_mix_with_audio, create_tags, duration, mock_upload_mix,
    upload_with_retries
)


@pytest.fixture
def with_description(testpath: Path) -> None:
    with (testpath / 'description.txt').open('w') as description_file:
        description_file.write('description\n')


@pytest.fixture
def with_tags(testpath: Path) -> None:
    create_tags(testpath)


@pytest.fixture
def with_too_many_tags(testpath: Path) -> None:
    create_tags(testpath, num_tags=6)


@pytest.fixture
def with_picture(testpath: Path) -> None:
    with (testpath / 'picture.png').open('w'):
        pass


@pytest.fixture
def empty_mix(testpath: Path) -> Mix:
    return create_empty_mix(testpath)


def test_create_mix_with_default_values(testpath: Path, empty_mix) -> None:
    check_mix_metadata(empty_mix, testpath.name, [], '')


def test_create_mix_with_tags(testpath: Path, with_tags) -> None:
    mix = create_mix_with_audio(testpath, tagged=False)
    check_mix_metadata(mix, 'Test', ['tag1', 'tag2'], '')


def test_strict_mix_without_tags_fails(testpath: Path) -> None:
    mix = create_mix_with_audio(testpath, tagged=True, strict=True)
    with pytest.raises(ValueError):
        mix.tags


def test_create_mix_with_tags_and_description(testpath: Path, with_description, with_tags) -> None:
    mix = create_mix_with_audio(testpath, tagged=False)
    check_mix_metadata(mix, 'Test', ['tag1', 'tag2'], 'description')


def test_strict_mix_without_description_fails(testpath: Path, with_tags) -> None:
    mix = create_mix_with_audio(testpath, tagged=True, strict=True)
    with pytest.raises(ValueError):
        mix.description


def test_create_mix_with_tags_description_picture_and_untagged_audio(
        testpath: Path, with_description, with_picture, with_tags
) -> None:
    mix = create_mix_with_audio(testpath, tagged=False)
    check_mix_metadata(
        mix, testpath.name, ['tag1', 'tag2'], 'description', 'picture.png'
    )
    assert not mix.valid


def test_strict_mix_without_picture_fails(testpath: Path, with_description, with_tags) -> None:
    mix = create_mix_with_audio(testpath, tagged=True, strict=True)
    with pytest.raises(ValueError):
        mix.picture


def test_create_mix_with_untagged_audio_gives_warning_if_strict_is_not_set(
        testpath: Path, with_description, with_tags, with_picture
) -> None:
    with patch('logging.warning') as warning:
        create_mix_with_audio(testpath, tagged=False, strict=False)
        warning.assert_called()


def test_create_mix_with_untagged_audio_fails_if_strict_is_set(
        testpath: Path, with_description, with_tags, with_picture
) -> None:
    with pytest.raises(ValueError):
        create_mix_with_audio(testpath, tagged=False, strict=True)


def test_create_valid_mix_with_tags_description_picture_and_tagged_audio(
        testpath: Path, with_description, with_tags, with_picture
) -> None:
    mix = create_mix_with_audio(testpath, tagged=True)
    check_mix_metadata(
        mix, testpath.name, ['tag1', 'tag2'], 'description', 'picture.png'
    )
    assert mix.valid


def test_create_mix_with_too_many_tags_invalid(
        testpath: Path, with_description, with_too_many_tags, with_picture
) -> None:
    mix = create_mix_with_audio(testpath, tagged=True)
    check_mix_metadata(
        mix, testpath.name,
        ['tag1', 'tag2', 'tag3', 'tag4', 'tag5', 'tag6'],
        'description', 'picture.png'
    )
    assert not mix.valid


def test_create_mix_with_too_many_tags_raises_if_strict(
        testpath: Path, with_description, with_too_many_tags, with_picture
) -> None:
    mix = create_mix_with_audio(testpath, tagged=True, strict=True)
    with pytest.raises(ValueError):
        check_mix_metadata(
            mix, testpath.name,
            ['tag1', 'tag2', 'tag3', 'tag4', 'tag5', 'tag6'],
            'description', 'picture.png'
        )


def test_create_mix_with_too_many_tracks_per_artist_invalid(testpath: Path) -> None:
    copytree(str(audio_path(tagged=True)), str(testpath), dirs_exist_ok=True)
    shutil.copyfile(testpath / '1s_silence.mp3', testpath / 'supernumerary.mp3')
    assert len(list(testpath.glob('*'))) == ALLOWED_TRACKS_PER_ARTIST + 1
    mix = create_empty_mix(testpath)
    assert not mix.valid


def test_create_mix_with_too_many_tracks_per_artist_raises_if_strict(testpath: Path) -> None:
    copytree(str(audio_path(tagged=True)), str(testpath), dirs_exist_ok=True)
    shutil.copyfile(testpath / '1s_silence.mp3', testpath / 'supernumerary.mp3')
    assert len(list(testpath.glob('*'))) == ALLOWED_TRACKS_PER_ARTIST + 1
    with pytest.raises(ValueError):
        create_empty_mix(testpath, strict=True)


def test_export_runs(testpath: Path, empty_mix: Mix) -> None:
    empty_mix.export()
    assert (testpath / 'mix.mp3').exists()
    assert duration(testpath / 'mix.mp3') < 1


def test_export_with_audio(testpath: Path, num_audio_files: int) -> None:
    mix = create_mix_with_audio(testpath, tagged=True)
    mix.export()
    assert (testpath / 'mix.mp3').exists()
    assert duration(testpath / 'mix.mp3') >= num_audio_files


@patch('logging.info')
def test_re_import_does_not_regenerate_mix(mock_info: Mock, testpath: Path) -> None:
    mix = create_mix_with_audio(testpath, tagged=True)
    mock_info.assert_called()
    mix.export()
    mock_info.reset_mock()
    create_mix_with_audio(testpath, tagged=True)
    mock_info.assert_called_once_with('Reading present %s', ANY)


def test_re_imported_mix_is_tagged_correctly(testpath: Path) -> None:
    mix = create_mix_with_audio(testpath, tagged=True)
    track_info = mix._track_info
    mix.export()
    mix = create_mix_with_audio(testpath, tagged=True)
    assert mix._track_info == track_info


def test_message_is_printed_with_verbose_flag(
        testpath: Path, with_description, with_picture
) -> None:
    mock_log = mock_upload_mix(testpath, **RESPONSE_OK)
    mock_log.assert_called_with('200: OK')


def test_tracks_are_printed_with_verbose_flag(
        testpath: Path, with_description, with_picture
) -> None:
    mock_log = mock_upload_mix(testpath, **RESPONSE_OK)
    print(testpath.glob('*'))
    for i, track in enumerate(
            sorted((Path(__file__).resolve().parent / 'data' / 'audio_tagged').glob('*'))
    ):
        mock_log.assert_has_calls([call('%s: %s (%s - %s)', i + 1, track.name, ANY, ANY)])


def test_export_message_is_printed_with_verbose_flag(
        testpath: Path, with_description, with_picture
) -> None:
    mock_log = mock_upload_mix(testpath, **RESPONSE_OK)
    mock_log.assert_has_calls([
        call(
            'Exporting %s audio to %s with bitrate %s kbps',
            mix_length(testpath / 'mix.mp3'), testpath / 'mix.mp3', MP3_KBIT_RATE
        )
    ])


def mix_length(path: Path) -> str:
    with audio_open(path) as audio:
        duration_seconds = int(audio.duration)
        return f'{duration_seconds // 60}:{duration_seconds % 60:02}'


def test_upload_message_is_printed_with_verbose_flag(
        testpath: Path, with_description, with_picture
) -> None:
    mock_log = mock_upload_mix(testpath, **RESPONSE_OK)
    mock_log.assert_has_calls([
        call(
            'Exporting %s audio to %s with bitrate %s kbps',
            mix_length(testpath / 'mix.mp3'), testpath / 'mix.mp3', MP3_KBIT_RATE
        )
    ])


def test_upload_crashes_without_export(testpath: Path, empty_mix: Mix) -> None:
    with pytest.raises(FileNotFoundError):
        empty_mix.upload()


def test_upload_success(testpath: Path, empty_mix: Mix):
    upload_with_retries(empty_mix, should_retry=False, **RESPONSE_OK)


def test_upload_other_error(testpath: Path, empty_mix: Mix):
    upload_with_retries(empty_mix, should_retry=False, **RESPONSE_500)


def test_upload_with_rate_limiting(testpath: Path, empty_mix: Mix) -> None:
    upload_with_retries(empty_mix, **RESPONSE_RATE_LIMITED)


def test_upload_with_connection_reset(testpath: Path, empty_mix: Mix) -> None:
    upload_with_retries(empty_mix, exc=requests.exceptions.ConnectionError, should_retry=False)


def test_upload_with_connection_reset_verbose(
        testpath: Path, with_description, with_tags, with_picture
) -> None:
    mix = create_mix_with_audio(testpath, tagged=True)
    upload_with_retries(mix, exc=requests.exceptions.ConnectionError, should_retry=False)


@pytest.mark.skipif(sys.version_info >= (3, 10), reason='mock_open seems broken on Python 3.10')
@pytest.mark.skipif(sys.version_info < (3, 8), reason='different usage of call_args.args')
@patch('io.open', side_effect=mock_open(read_data=MOCK_ACCESS_TOKEN))
@patch.object(Path, 'exists', return_value=True)
def test_get_access_token_with_file_in_current_dir(_: Mock, mock_file: Mock, testpath: Path):
    get_access_token()
    mock_file.assert_called_once()
    assert Path(ACCESS_TOKEN_FILE) in mock_file.call_args.args


@pytest.mark.skipif(sys.version_info >= (3, 10), reason='mock_open seems broken on Python 3.10')
@pytest.mark.skipif(sys.version_info < (3, 8), reason='different usage of call_args.args')
@patch('io.open', side_effect=mock_open(read_data=MOCK_ACCESS_TOKEN))
@patch.object(Path, 'exists', side_effect=[False, True])
def test_get_access_token_with_file_in_home_dir(_: Mock, mock_file: Mock, testpath: Path):
    get_access_token()
    mock_file.assert_called_once()
    assert Path.home() / ACCESS_TOKEN_FILE in mock_file.call_args.args


@pytest.mark.skipif(sys.version_info >= (3, 10), reason='mock_open seems broken on Python 3.10')
@pytest.mark.skipif(sys.version_info < (3, 8), reason='different usage of call_args.args')
@patch('io.open', side_effect=mock_open(read_data=MOCK_ACCESS_TOKEN))
@patch.object(Path, 'exists', side_effect=[False, False, True])
def test_get_access_token_with_file_in_config_dir(_: Mock, mock_file: Mock, testpath: Path):
    get_access_token()
    mock_file.assert_called_once()
    assert Path.home() / '.config' / 'media-tools' / ACCESS_TOKEN_FILE in mock_file.call_args.args


@pytest.mark.skipif(sys.version_info >= (3, 10), reason='mock_open seems broken on Python 3.10')
@pytest.mark.skipif(sys.version_info < (3, 8), reason='different usage of call_args.args')
@patch('io.open', side_effect=mock_open(read_data=MOCK_ACCESS_TOKEN))
def test_get_access_token_with_explicit_file(mock_file: Mock, testpath: Path):
    get_access_token(Path('mock path'))
    mock_file.assert_called_once()
    assert Path('mock path') in mock_file.call_args.args


@patch.object(Path, 'exists', return_value=False)
def test_get_access_token_fails_if_file_not_present(exists: Mock, testpath: Path):
    with pytest.raises(AuthorizationError):
        get_access_token()
    exists.assert_called()


@patch('io.open', side_effect=FileNotFoundError)
def test_get_access_token_with_explicit_but_nonexistent_file(_: Mock, testpath: Path):
    with pytest.raises(AuthorizationError):
        get_access_token(Path('mock path'))
