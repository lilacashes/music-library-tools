__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

from os import listdir
from pathlib import Path

import pytest

from media_tools.util.playlist_copier import PlaylistCopier, find_file_by_name


def test_copy_playlist(copier: PlaylistCopier, testpath: Path) -> None:
    copier.copy_playlist(2, testpath)
    copied_files = listdir(testpath)
    assert 2 == len(copied_files)
    assert '01 - Test A.mp3' in copied_files
    assert 'Test B.mp3' in copied_files


def test_copy_playlist_honors_length_limit(copier: PlaylistCopier, testpath: Path) -> None:
    copier.copy_playlist(1, testpath)
    copied_files = listdir(testpath)
    assert 1 == len(copied_files)
    assert '01 - Test A.mp3' in copied_files
    assert 'Test B.mp3' not in copied_files


def test_copy_playlist_copies_all_if_length_unset(copier: PlaylistCopier, testpath: Path) -> None:
    copier.copy_playlist(0, testpath)
    copied_files = listdir(testpath)
    assert len(copied_files) == 5


def test_copy_playlist_with_renumber(copier: PlaylistCopier, testpath: Path) -> None:
    copier.copy_playlist(2, testpath, renumber=True)
    copied_files = listdir(testpath)
    assert 2 == len(copied_files)
    assert '01 - Test A.mp3' in copied_files
    assert '02 - Test B.mp3' in copied_files
    assert 'Test B.mp3' not in copied_files


def test_copy_playlist_ignores_nonexistent_file(copier: PlaylistCopier, testpath: Path) -> None:
    copier.copy_playlist(0, testpath)
    copied_files = listdir(testpath)
    assert 'NONEXISTENT.mp3' not in copied_files


def test_copy_playlist_with_nonexistent_file_returns_correct_length(
        copier: PlaylistCopier, testpath: Path
) -> None:
    copier.copy_playlist(3, testpath)
    copied_files = listdir(testpath)
    assert 3 == len(copied_files)


def test_copy_playlist_with_nonexistent_file_renumbers_correctly(
        copier: PlaylistCopier, testpath: Path
) -> None:
    copier.copy_playlist(3, testpath, renumber=True)
    copied_files = listdir(testpath)
    assert '03 - Test D.mp3' in copied_files


@pytest.fixture
def data_path(local_path: Path) -> Path:
    return local_path / 'data/find_file_by_name'


def test_find_file_by_name(data_path) -> None:
    assert find_file_by_name('a', data_path) == data_path / 'sub_a' / 'a'
    assert find_file_by_name('b', data_path) == data_path / 'sub_b' / 'b'
    assert find_file_by_name('a', data_path / 'sub_b') == data_path / 'sub_b' / 'a'
    assert find_file_by_name('c', data_path) is None
