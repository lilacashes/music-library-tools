
MEDIA_EXTENSIONS = (
    'mp3', 'ogg', 'flac', 'wav', 'm4a', 'm4b', 'm4p', 'aif', 'aiff', 'ape', 'spx', 'wv',
    'mkv', 'avi', 'mp4', 'm4v', 'mov', 'wmv', 'flv', 'webm'
)
