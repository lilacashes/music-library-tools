ARG PYTHON_VERSION=latest
FROM python:$PYTHON_VERSION

RUN apt-get -y -qq update
RUN apt-get -y -qq upgrade
RUN apt-get -y -q install --fix-missing jq ffmpeg > /dev/null

# Create virtual environment.
ARG VENV=media-tools
RUN mkdir -p /$VENV/venv
RUN python -m venv /$VENV/venv

# Activate the venv.
ENV VIRTUAL_ENV /$VENV/venv
ENV PATH "$VIRTUAL_ENV/bin:${PATH}"

# Install poetry into the venv.
RUN pip install -q poetry

# Install dependencies into the venv.
COPY poetry.lock pyproject.toml /$VENV/
WORKDIR /$VENV
RUN poetry install

CMD bash
